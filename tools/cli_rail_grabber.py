"""
This script communicates with a device via serial port.
It repeatedly prompts the user for a value and sends it to the device.

Functionality:
- Automatically selects the first device in /dev/serial/by-id/
- Establishes a serial connection
- Continuously prompts the user for input
- Sends the input value to the device using a specific command format
- Handles serial communication errors
- Attempts to reconnect if the connection is lost

Constraints:
- Requires pyserial library
- Uses the first device found in /dev/serial/by-id/
- Uses specific serial communication parameters
- Implements error handling and reconnection attempts
"""

import serial
import time
import os
import glob

def get_serial_port():
    devices = glob.glob('/dev/serial/by-id/*')
    if devices:
        return os.path.realpath(devices[0])
    return None

def create_serial_connection():
    port = get_serial_port()
    if not port:
        print("No serial device found in /dev/serial/by-id/")
        return None
    
    try:
        ser = serial.Serial(port, 9600, timeout=10,    
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS)
        time.sleep(1)
        print(f"Connected to {port}")
        return ser
    except serial.SerialException as e:
        print(f"Failed to connect to the serial port: {e}")
        return None

def send_command(ser, value):
    try:
        time.sleep(0.3)
        cmd = f"111 {value} 222\n"
        print(f"Sending command: {cmd.strip()}")
        ser.write(cmd.encode('utf-8'))
        ser.flush()
    except serial.SerialException as e:
        print(f"Error sending command: {e}")
        return False
    return True

if __name__ == '__main__':
    ser = create_serial_connection()
    if not ser:
        print("Exiting due to connection failure.")
        exit(1)

    while True:
        value = input("Enter a value (or 'q' to quit): ")
        if value.lower() == 'q':
            break
        try:
            int_value = int(value)
            if not send_command(ser, int_value):
                print("Attempting to reconnect...")
                ser.close()
                ser = create_serial_connection()
                if not ser:
                    print("Reconnection failed. Exiting.")
                    break
        except ValueError:
            print("Invalid input. Please enter an integer.")

    if ser:
        ser.close()
