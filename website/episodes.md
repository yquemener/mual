% MUAL: Micro-Usine Automatisée Libre



Épisode 0: Luz'in et l'idée
------------------------------------------


<div class="clearfix">

C'est à [Luz'In](https://www.luzin.net/)
Fablab isérois situé à la Tour du Pin, qu'est née l'idée de créer un dispositif d'assemblage automatique. Lieu de fabrication numérique, Luz'in produit de nombreuses pièces, parfois complexes, grâce à sa fraiseuse numérique, sa découpeuse laser ou ses imprimantes 3D, mais passe beaucoup de temps à transporter matière et pièces, à assembler les objets finaux.
![](img/luzin.jpg)
</div>

<div class="clearfix">
![](img/factory2.png)
En parallèle, un questionnement sur les raisons du faible succès du mouvement open hardware en comparaison du mouvement open source, pointait du doigt la difficulté à reproduire les projets pourtant publiés sur internet, et la possibilité pour une machine d'assemblage d'aider au développement de cette communauté.
</div>

Épisode 1: L'Ademe et le projet vhélio
-----------------------------------------------------------

<div class="clearfix">
![Logo de l'eXtrême Défi](img/xdlogo.webp "Logo de l'eXtrême Défi")
C'est à ce moment que nos efforts ont rencontré ceux de l'ADEME, l'agence pour la transition écologique et de son effort [Extrême Défi](https://xd.ademe.fr/) qui vise à proposer de nouvelles mobilités. Elle a depuis plusieurs années lancé un concours d'innovation pour la création de véhicules intermédiaires. Ce concours a donné naissance à plus [d'une centaine de projets](https://wiki.lafabriquedesmobilites.fr/index.php?title=Sp%C3%A9cial:Requ%C3%AAter&limit=250&offset=10&q=%5B%5BCat%C3%A9gorie%3AVehicule%5D%5D+%5B%5BD%C3%A9fi%3A%3AL%27extr%C3%AAme+d%C3%A9fi+ADEME%5D%5D&p=mainlabel%3D%2Fformat%3Dbroadtable%2Flink%3Dall%2Fheaders%3Dshow%2Fsearchlabel%3D-26hellip-3B-20autres-20r%C3%A9sultats%2Fdefault%3Daucun-20%3A-2D%28-20%C3%A0-20vous-20de-20jouer-20%21%2Fclass%3Dsortable-20wikitable-20smwtable%2Fsep%3D%2C-26ensp-3B&po=%3FUtilisateur%3DContributeurs%0A%3FModification+date%0A&sort=Modification+date&order=descending&eq=no#search)
![ADEME](img/ademe.webp "agence pour la transition écologique")
</div>


<div class="clearfix">
![Vhélio - Le tricyle ouvert de l'association "Vélo solaire pour tous" (© Vhélio 2023)](img/vhelio.jpg "Vhélio - Le tricyle ouvert de l'association "Vélo solaire pour tous" (© Vhélio 2023)")
Parmi ces projets, un tricyle solaire à assistance électrique nommé [«Vhélio»](https://vhelio.org/) a attiré l'attention des organisateurs. Réalisé par une association à but non-lucratif ("Vélo solaire pour tous"), il est entièrement ouvert, les plans sont fournis gratuitement et sont librement modifiables pour qui a envie de s'emparer du projet, de la production, des versions suivantes, et ce avec la bénédiction des concepteurs. L'ADEME a donc proposé d'en faire l'objet de son appel à projet suivant: «Usine Distribuée», qui vise à financer des prototypes de petites usines permettant de répartir sur tout le territoire la production de tels véhicules. C'est dans le cadre de cet appel à projet que nous avons désormais quelques financements et un objectif clair: assembler de façon autonome des chassis de vhélios.

</div>

  
Épisode 2: Armstone, La recherche
---------------------------------

<div class="clearfix">
La taille de la chose à produire étant plus grande que ce que nous avions initialement prévu (de la petite électronique, de petits assemblages), il nous fallut concevoir un nouvel ensemble robotique. C'est en explorant les possibilités d'acheter des bras robotique d'occasion que nous sommes tombés sur l'annonce d'un chercheur en robotique qui vendait des éléments de ses projets de recherche terminés. Nous avons trouvé un robot qui ressemblait très fortement à ce que nous étions en train de concevoir: le [robot Armstone](http://www.julius-sustarevas.com/2022-07-01-armstone-at-reddit-robotics-showcase/), une base mobile équipée d'un bras 6 axes.
![Le robot Armstone - (© Julius-Sustarevas 2022)](img/armstone.jpg "Le robot Armstone - (© Julius-Sustarevas 2022)")
</div>

<div class="clearfix">
![](img/robotarm1.jpg "Armstone arrivé en France")
  Julius Sustarevas, chercheur basé à Londres, a développé ce robot pour étudier la faisabilité de l'impression 3D pour l'architecture. Un extrudeur d'argile monté en bout du bras, il a exploré différentes techniques de positionnement pour atteindre son objectif. En modifiant son outillage et en ajoutant des briques logicielles, il pourrait former la partie mobile de MUAL. À condition toutefois de le dupliquer. Séduit par notre projet, et heureux à la perspective de voir son robot continuer à défricher de nouveaux usages, il a accepté de mettre sous une licence ouverte les éléments qui ne l'étaient pas encore. Nous sommes allés l'acheter à Londres, et avons commencé à l'adapter à notre usage à Luz'in.
</div>
  

Épisode 3: Le deuxième robot et le problème du open hardware
--------------------------------------------

<div class="clearfix">
  Nous nous lançons donc dans la duplication de la base mobile Armstone! Et nous nous heurtons au problème même que MUAL vise à résoudre: même en open hardware, la duplication d'un tel project est compliquée et longue. Il nous faudra plusieurs allers-retours auprès de fournisseurs différents pour obtenir toutes les pièces. Comme il est classique dans la duplication d'un tel projet, il s'agit plus d'une nouvelle version que d'une copie: les ordinateurs embarqués sont plus performants, les supports des axes moteurs en métal au lieu d'être imprimés en 3D, par contre les moteurs sont moins puissants. Le bras est d'une génération suivante, plus précis mais demandant 48V au lieu des 24V de la première version. Nous avons décidé de ne pas équiper le nouveau de LIDARs comme l'était l'ancien, avant d'avoir plus exploré les alternatives.
![](img/montagebichon1.jpg "")
</div>

<div class="clearfix">
![](img/poupetteetbichon.jpg "")
Nous avons fini l'assemblage de la base juste à temps pour une démonstration à un événement de la CCI Nord-Isère qui aura vu pour la première fois les deux Armstones (noms de code: Poupette pour l'aînée et Bichon pour le cadet). Cela a été l'occasion de tester un peu l'endurance de notre mécanique. Nous avons pu constater qu'avec les 8 kilos en plus de la nouvelle version du bras, nos axes de roues étaient un peu faibles et allons les renforcer.
</div>


Épisode 4: YOLO! Deep learning et OpenSource
-------------------------------------------------------------
![](img/vision3.jpg)

Une des choses qui rend possible aujourd'hui des placements aussi précis sur un volume de travail de plusieurs mètres carrés sont les progrès de la vision par ordinateur, domaine récemment boosté par l'arrivée des techniques de deep learning. Un des membres de notre équipe étant spécialiste du domaine, il a commencé à accumuler des images d'exemple afin de pouvoir spécialiser un modèle ouvert connu sous le nom de [YOLO ("You Only Look Once")](https://github.com/ultralytics/yolov5).


<div class="clearfix">
Équipé de plusieurs caméras, le bras est capable de détecter la position de son outil ainsi que celle de sa cible et de se diriger automatiquement vers cette dernière. La tolérance mécanique dans l'opération d'insertion d'une vis dans un trou percé est de l'ordre de 0.5 mm. Nos bras étant précis à 0.1 mm, ils peuvent accomplir cette tâche, à condition d'être bien guidés!
![](img/vision2.jpg)
</div>  

Épisode 5: Mise au point de la pince
-------------------------------------------------------------------------------


