% MUAL: Micro-Usine Automatisée Libre
(Micro-Automated Free Factory)

[FR 🇫🇷](index.html)

* Micro: Affordable on a family budget
* Factory: The goal is to manufacture objects
* Automated: It does not require human labor
* Free: The machines are under licenses adhering to the free philosophy

<img src='img/factory2.png' height=400px>

How?
====

It involves connecting an inventory of parts and materials to one or more manufacturing machines and an assembly area.

- **Transport Robot**: a mobile base equipped with an arm capable of reaching all work areas of the machines and inventory shelves.
- **Manufacturing Machines**: cutters, milling machines, 3D printers. There are many open hardware designs. I plan to use a numerical milling machine for the demonstrator.
- **Assembly Table**: the manufactured parts and stock parts are brought here to be assembled by several arms equipped with different tools (screwdrivers, soldering irons, specialized equipment)
- **Stock**: a shelf where parts and raw materials are loaded and stored in a way that allows easy restocking

We will maximize the reuse of existing open hardware designs and not reinvent the wheel. There are designs for rolling bases, arms, and manufacturing machines under OSHW licenses.

Why?
====

To assemble small electronics, appliances, small robotics, light industrial equipment.

An intermediate goal would be to be able to assemble several of its elements (CNC, arms and mobile bases).

ADEME Supports Our First Iteration
==================================

This project was accepted by ADEME as part of the ["Extreme Défi, Distributed Factory"](https://agirpourlatransition.ademe.fr/entreprises/aides-financieres/20221102/usine-distribuee-lextreme-defi-xd)(FR) call for projects.

Goal: to assemble intermediate vehicles (between the electric bicycle and the small electric car) participating in the extreme challenge. We are focusing on the [vhéliotech](https://vhelio.org/vheliotech/)(FR) which is a fully documented open-hardware vehicle.

We will have funding that allows us to buy semi-pro arms:
- uFactory xArm 6
- Universal Robots UR5e

Unfortunately, they are not open source but will allow rapid prototyping with solid arms.

For the mobile base, we will either reproduce/imitate a project from the FabAcademy: [Utility](http://fabacademy.org/2021/labs/agrilab/students/theo-gautier/projects/final-project/) or duplicate [Armstone, a reasearch project](http://www.julius-sustarevas.com/2022-09-01-armstone-sale/) 


The Goal is a Project as Open as Possible
=========================================

List of projects that can contribute:

OSHW Robot Arm Projects:
- [MeArm](https://mearm.com/2019/06/14/looking-to-purchase-a-mearm-or-find-out-more/)
- Robotis' [Open Manipulator-X](https://www.robotis.us/openmanipulator-x/)
- [ArmUno 2.0](https://microbotlabs.com/robot-kits.html#armuno2-robotic-arm-kit-available)
- [Bajdi's stepper robot arm](http://www.bajdi.com/stepper-robot-arm/)

Arms with a Non-Commercial Clause:

- [Open Assistive Arm For Meals (oaafm)](https://hackaday.io/project/172170-open-assistive-robotic-arm-for-meal)
- [EEZYbotARM MK2](https://www.thingiverse.com/thing:1454048)


Candidates for the Mobile Base:

- [Utility](http://fabacademy.org/2021/labs/agrilab/students/theo-gautier/projects/final-project/), a follower robot for the farm
- Robotis' [Turtlebot](https://www.robotis.us/turtlebot-3/)
