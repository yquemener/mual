% MUAL: Micro-Usine Automatisée Libre

<nav>
MUAL:  [Presentation](index.html) -- [Historique](episodes.html)
</nav>



* Micro: Abordable sur un budget familial
* Usine: Le but est de fabriquer des objets
* Automatisée: Elle ne requiert pas de travail humain
* Libre: Les machines sont sous des licences obéissant à la philosophie du libre


Épisode 0: La communauté, Luz'in et l'idée
------------------------------------------

  La communauté. Un mot qui a beaucoup d'importance pour qui a comme nous baigné dans le monde des fablab, de hackerspaces, des makerspaces, de ce qu'on appelle aujourd'hui des tiers lieux. Plusieurs thèmes guident les gens qui les habitent. À Luz'In, un fablab isérois, un des thèmes récurrents est la démocratisation des outils de fabrication. 
  
  Des initiations, des machines de fabrication numérique à disposition, oui, mais aussi un questionnement sur l'étape suivante. Une chose nous chagrinait: beaucoup de ces lieux conçoivent des choses, souvent utiles et Internet regorge de projets "open hardware", conçus par des passionés qui mettent les plans et les instructions en ligne à disposition de tous. Malheureusement, ces projets demandent souvent des compétences pointues et beaucoup de temps pour être dupliquées.
  
  L'idée est venue de créer une machine d'assemblage qui puisse faire le lien entre plusieurs machines au sein d'un fablab et réaliser des assemblages électroniques, robotiques, électro-ménagers. Le projet MUAL était né: Micro-Usine Automatisée et Libre.
  

Épisode 1: L'Ademe et le projet (vhélio), Les fonds publics
-----------------------------------------------------------

  Le support public est arrivé assez naturellement via l'ADEME, l'agence de la transition écologique, qui cherchait un moyen d'organiser la production de vélos électriques solaires (les vhélio) d'une façon décentralisée. Notre idée en gestation, qui imaginait des assemblages de petits systèmes a été adaptée pour l'assemblage de ces vélos électriques (des trikes en fait).
  
  Désormais munis d'un budget et d'un objectif, il nous était possible d'avancer bien plus rapidement. Plutôt que de concevoir un bras et une base open hardware, nous avons pu nous tourner vers des solutions "sur étagère", remettant à plus tard l'utilisation de composants complètement ouverts mais rapprochant d'autant la première version de la micro-usine.
  
  La première production de MUAL sera donc un chassis de vhélio. Un trike électrique solaire open hardware, conçu par des passionnés dans l'optique de créer des véhicules alternatifs. Tout un programme en soi, que nous avons pu découvrir au salon du véhicule alternatif où nous avait invité l'ADEME. Une grande foire aux bricoleurs où se cotoient bidouilleurs vélocipédistes et petits entrepreneurs de mini-véhicules électriques. Nous en sommes revenus l'esprit en ébullition.
  
  Ce chassis est un assemblage de tubes à section carée en aluminium, vissés les uns aux autres. Notre but principal dans ce projet est donc de parvenir à aligner les trous de deux tubes différents, d'y insérer une vis et d'y serrer un écrou. Plus facile à dire qu'à faire! La tolérance pour ce genre de tâches est de moins de 0.5 mm.
  
  Pouvant nous permettre d'acheter du matériel pro/semi-pro, nous avons choisi un bras xArm6 produit par uFactory et promettant une précision de 0.1 mm, normalement suffisante pour la tâche. Et cette recherche nous a réservé une surprise...
  
Épisode 2: Armstone, La recherche
---------------------------------

  La recherche scientifique. En cherchant la possibilité d'acheter un bras d'occasion, une bonne fortune nous est arrivée. Nous sommes tombé sur l'annonce d'un chercheur en robotique qui tentait de recouper les couts de développement d'un de ses projets de recherche utilisant le bras qui nous intéresse et nous avons découvert que ce bras était attaché à exactement le type de base mobile que nous souhaitions développer.
  
  Julius Sustarevas, localisé à Londres, a développé ce robot nommé Armstone pour étudier la faisabilité de l'impression 3D pour l'architecture. Un extrudeur d'argile monté en bout du bras, il a exploré différentes techniques de positionnement pour atteindre son objectif. Sa thèse finie, il s'apprêtait à mettre en vente son robot en pièces détachées. Nous l'avons racheté dans son ensemble (l'extrudeur d'argile en moins), tant il correspondait à nos besoins.
  
  Il a été séduit par le projet et voyant que nous aurions besoin de le dupliquer, a accepté de le mettre sous une license open hardware. La perspective de voir son robot continuer à défricher de nouveaux usages est également enthousiasmante pour un chercheur! Nous sommes allés le chercher à Londre, avons beaucoup échangé sur ses capacités, ses points d'amélioration, et avons particulièrement été séduit par ses roues holonomiques qui permettaient une grande flexibilité de trajectoires, ainsi que par le fait que les contributions de Julius sur le contrôle d'un tel arrangement étaient open source.
  
  Le monde de la recherche et de l'open source sont souvent extrêmement proches au niveau des mentalités, nous avons gagné plusieurs mois de développement grâce à lui et nous continueront de nommer sa plateforme "Armstone".
  
  
Épisode 3: Bichon, Armstone 2, open hardware
--------------------------------------------

  Nous nous lançons dans la duplication de la base mobile Armstone! Et nous nous heurtons au problème même que MUAL vise à résoudre: même en open hardware, la duplication d'un tel project est compliquée et longue. Après plusieurs commandes nécessaires auprès de fournisseurs différents pour obtenir toutes les pièces, et plusieurs erreurs d'assemblages plus tard, nous pouvons vous montrer le premier rejeton et deuxième exemplaire des Armstone.
  
  Plus qu'une duplication, c'est plutôt une nouvelle version. Les ordinateurs embarqués sont plus performants, les supports des axes moteurs en métal au lieu d'être imprimés en 3D, par contre les moteurs sont moins efficaces. Le bras est d'une génération suivante, plus précis mais demandant 48V au lieu des 24V de la première version. Nous avons décidé de ne pas équiper le nouveau de LIDARs comme l'était l'ancien, avant d'avoir plus exploré les alternatives.
  
  C'est un classique dans la duplication des projets open hardware: il est rare que les pièces identiques soient disponibles, et les compétences requises pour les remplacer font que l'on est souvent obligé d'adapter, d'améliorer. 
  
  Sommés de nommer nos robots pour les distinguer et refusant de céder à la facilité des numéros, nous les avons nommés Poupette (le premier) et Bichon (le deuxième).


Épisode 4: YOLO! Analyse d'image, deep learning et OpenSource
-------------------------------------------------------------

  Une des choses qui rend possible aujourd'hui des placements aussi précis sur un volume de travail important de plusieurs mètres carrés sont les progrès de la vision par ordinateur, domaine récemment boosté par l'arrivée des techniques de deep learning.

Épisode 5: La pince. Conception, création, tests de saisie et de positionnement
-------------------------------------------------------------------------------


