% MUAL: Micro-Usine Automatisée Libre



* Micro: Abordable sur un budget familial
* Usine: Le but est de fabriquer des objets
* Automatisée: Elle ne requiert pas de travail humain
* Libre: Les machines sont sous des licences obéissant à la philosophie du libre


![Essai de l'outil de préhension des rails](img/assembly1.jpg "Base mobile équipée d'un bras assemblant des rails métalliques")

Le but de MUAL est de créer un ensemble de robots capables d'assembler des objets.

Nous avons aujourd'hui deux robots mobiles, équipés de bras qui seront équipés de multiples outils de préhension et de serrage. 

Une table de montage orientable est en cours de conception.


![Démonstration aux journées de l'innovation à la CCI Nord-Isère](img/assembly2.jpg "Deux robots collaborant à un assemblage")

MUAL sera un dispositif entièrement automatisé capable de placer, aligner et visser les pièces d'un montage selon un plan pré-établi.

![Outil d'apprentissage du logiciel de guidage visuel](img/vision2.jpg "GUI d'un outil de deep learning permettant la localisation de trous et de vis dans une image video")
