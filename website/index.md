% MUAL: Micro-Usine Automatisée Libre

* Micro: Abordable sur un budget familial
* Usine: Le but est de fabriquer des objets
* Automatisée: Elle ne requiert pas de travail humain
* Libre: Les machines sont sous des licences obéissant à la philosophie du libre

![](img/onshape.png)

<span class="bandeau-partenaires">
<a href="https://www.luzin.net">![](https://www.luzin.net/wp-content/uploads/2018/04/luzin_logo.jpg)</a>
<a href="https://www.luzin.net">![](http://www.iv-devs.com/title.png)</a>
<a href="https://vhelio.org">![](https://vhelio.org/wp-content/uploads/2021/02/Vhelio_Logo.png)</a>
<a href="https://xd.ademe.fr/">![ADEME](https://custom-images.strikinglycdn.com/res/hrscywv4p/image/upload/c_limit,fl_lossy,h_300,w_300,f_auto,q_auto/9163851/68302_889425.png "agence pour la transition écologique")
<span>
