class Poupette:
    esc_names = {
        'fl': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:5.1:1.0',
        'fr': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:5.2:1.0',
        'bl': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:5.3:1.0',
        'br':  '/dev/serial/by-path/pci-0000:00:14.0-usb-0:5.4:1.0'}
    external_ip = "192.168.2.1"
    video_ip = "192.168.2.2"  # IP address for video streaming
    wheel_radius = 0.08
    length_x = width = 0.285
    length_y = length = 0.32
    encoder_tick_per_turn = 4400/.94
    video_port = 8445
    arm_ip = "192.168.2.100"  # Ajout de l'adresse IP du bras
    specific_device = '/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0'


class Bichon:
    esc_names = {
        'fl': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:7.2:1.0',
        'fr': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:7.1:1.0',
        'bl': '/dev/serial/by-path/pci-0000:00:14.0-usb-0:7.3:1.0',
        'br':  '/dev/serial/by-path/pci-0000:00:14.0-usb-0:7.4.3:1.0'}
    external_ip = "192.168.3.1"
    video_ip = "192.168.3.2"  # IP address for video streaming
    wheel_radius = 0.08
    length_x = width = 0.285
    length_y = length = 0.32
    encoder_tick_per_turn = 4400/.94
    video_port = 8445
    arm_ip = "192.168.3.100"  # Ajout de l'adresse IP du bras
    specific_device = '/dev/serial/by-id/usb-1a86_USB2.0-Ser_-if00-port0'

    

class Orchestrator:
    external_ip = "192.168.8.213"

ZMQ_PORT = 5566
