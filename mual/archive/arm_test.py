#!/usr/bin/env python3

import time
from xarm.wrapper import XArmAPI

arm = XArmAPI("192.168.3.100")
arm.motion_enable(enable=True)
print('motion_enable')
arm.set_mode(5)
print('set_mode')
arm.set_state(state=0)
print('set_state')
arm.reset(wait=False)
print('arm_reset')

#ret = arm.set_position(*poses[0], speed=10, mvacc=100, wait=False)
p = arm.position
# arm.set_position(x=p[0]+20)
arm.vc_set_cartesian_velocity(speeds=[60,0,0,0,0,0], duration=1)
time.sleep(2)
arm.vc_set_cartesian_velocity(speeds=[-60,0,0,0,0,0], duration=1)
time.sleep(2)
arm.set_mode(0)
arm.set_state(state=0)

#arm.set_position(x=p[0]+40)
#print('set_position, ret: {}'.format(ret))
arm.reset(wait=True)
arm.disconnect()
