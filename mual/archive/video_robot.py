import socket
import threading
import subprocess
import time

class ArmstoneRobot:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.running = False
        self.video_process = None

    def start_video_stream(self):
        cmd = [
            "v4l2-ctl", "-d", "/dev/video0", "-c", "focus_auto=0",
            "&&", "v4l2-ctl", "-d", "/dev/video0", "-c", "focus_absolute=40",
            "&&", "v4l2-ctl", "-d", "/dev/video0", "-v", "width=640,height=480,pixelformat=MJPG",
            "--stream-mmap", "--stream-to=-", "|", "nc", self.host, str(self.port)
        ]
        self.video_process = subprocess.Popen(" ".join(cmd), shell=True)

    def stop_video_stream(self):
        if self.video_process:
            self.video_process.terminate()
            self.video_process = None

    def handle_command(self, command):
        if command == "START":
            if not self.running:
                self.start_video_stream()
                self.running = True
                return "Video stream started"
            else:
                return "Video stream is already running"
        elif command == "STOP":
            if self.running:
                self.stop_video_stream()
                self.running = False
                return "Video stream stopped"
            else:
                return "Video stream is not running"
        else:
            return "Unknown command"

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(('0.0.0.0', 8000))
            s.listen()
            print(f"Robot listening on port 8000")
            while True:
                conn, addr = s.accept()
                with conn:
                    print(f"Connected by {addr}")
                    while True:
                        data = conn.recv(1024)
                        if not data:
                            break
                        command = data.decode('utf-8')
                        response = self.handle_command(command)
                        conn.sendall(response.encode('utf-8'))

    def run_with_reconnect(self):
        while True:
            try:
                self.run()
            except Exception as e:
                print(f"Connection lost: {e}")
                print("Attempting to reconnect in 5 seconds...")
                time.sleep(5)

if __name__ == "__main__":
    robot = ArmstoneRobot('192.168.1.135', 8445)  # Remplacez par l'IP du PC d'orchestration
    robot.run_with_reconnect()
