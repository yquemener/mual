#!/bin/bash


sudo rmmod v4l2loopback
sudo modprobe v4l2loopback video_nr=32,33  card_label="Virtual Camera"
nc -l 8444 | ffmpeg -f mjpeg -i - -vcodec copy -f v4l2 /dev/video32 &
nc -l 8445 | ffmpeg -f mjpeg -i - -vcodec copy -f v4l2 /dev/video33 &
sleep 1
ffplay /dev/video32 &
ffplay /dev/video33 &

