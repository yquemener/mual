import socket
import threading
import subprocess
import time

class Orchestrator:
    def __init__(self, robot_ip, robot_port, video_port):
        self.robot_ip = robot_ip
        self.robot_port = robot_port
        self.video_port = video_port
        self.video_process = None

    def send_command(self, command):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.robot_ip, self.robot_port))
            s.sendall(command.encode('utf-8'))
            response = s.recv(1024)
            return response.decode('utf-8')

    def start_video_receiver(self):
        cmd = [
            "nc", "-l", str(self.video_port), "|",
            "ffmpeg", "-f", "mjpeg", "-i", "-", "-vcodec", "copy", "-f", "v4l2", "/dev/video32",
            "&&", "ffplay", "/dev/video32"
        ]
        self.video_process = subprocess.Popen(" ".join(cmd), shell=True)

    def stop_video_receiver(self):
        if self.video_process:
            self.video_process.terminate()
            self.video_process = None

    def check_connection(self):
        while True:
            if self.video_process and self.video_process.poll() is not None:
                print("Video stream stopped unexpectedly. Restarting...")
                self.stop_video_receiver()
                time.sleep(1)
                self.send_command("START")
                self.start_video_receiver()
            time.sleep(5)

    def run(self):
        threading.Thread(target=self.check_connection, daemon=True).start()
        while True:
            command = input("Enter command (START/STOP/QUIT): ").upper()
            if command == "QUIT":
                break
            elif command in ["START", "STOP"]:
                response = self.send_command(command)
                print(f"Robot response: {response}")
                if command == "START":
                    self.start_video_receiver()
                elif command == "STOP":
                    self.stop_video_receiver()
            else:
                print("Invalid command")

if __name__ == "__main__":
    orchestrator = Orchestrator('192.168.1.100', 8000, 8445)  # Remplacez par l'IP du robot
    orchestrator.run()
