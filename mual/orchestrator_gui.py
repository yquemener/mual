#!/usr/bin/env python


"""
ZM UI Application

Fonctionnalités :
1. Interface graphique pour contrôler et surveiller un robot mobile
2. Affichage des données du robot dans une arborescence
3. Contrôle manuel de la position du robot
4. Visualisation des logs de données
5. Basculement entre le contrôle par joystick et le contrôle par réseau
6. Boutons Stop et Enable pour contrôler l'état d'activation du robot

Contraintes :
1. Utilisation de PyQt5 pour l'interface graphique
2. Communication via ZeroMQ pour l'envoi et la réception des données
3. Affichage des données en temps réel
4. Possibilité de basculer entre le contrôle par joystick et par réseau
5. Un seul paramètre 'enable' contrôle l'état d'activation du robot
6. Le robot s'arrête complètement (RPM à 0) lorsque 'enable' est à 0
"""


import time
import math
import netifaces

import sys
import yaml
import zmq
import threading
import matplotlib.pyplot as plt
import numpy as np
import pickle
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QColor, QBrush
from PyQt5.QtWidgets import QApplication

from PyQt5 import uic
from PyQt5.QtCore import pyqtSignal, Qt, QTimer
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QImage, QPixmap, QCursor
from PyQt5.QtWidgets import QApplication, QPushButton, QHBoxLayout, QWidget, QLabel, QCheckBox, QMenu, QListWidgetItem
import cv2
import subprocess

# Hack to make PyQt and cv2 load simultaneously
import os
from pathlib import Path
import PyQt5
os.environ["QT_QPA_PLATFORM_PLUGIN_PATH"] = os.fspath(
    Path(PyQt5.__file__).resolve().parent / "Qt5" / "plugins"
)


from mual import conf

try:
    from mual.vision.inference import InferenceModule
    VISION_AVAILABLE = True
except ImportError:
    VISION_AVAILABLE = False


import warnings
warnings.filterwarnings('ignore', message='.*torch.cuda.amp.autocast.*')


class App(QApplication):
    tree_update = pyqtSignal(str, str)
    log_finished = pyqtSignal()
    def __init__(self):
        super().__init__([])

        self.debug_mode = False
        self.check_ip_and_set_mode()

        # Initialiser le contexte ZMQ et les sockets AVANT toute autre initialisation
        self.zmq_context = zmq.Context()
        self.pub_socket = self.zmq_context.socket(zmq.PUB)
        self.pub_socket.bind(f"tcp://0.0.0.0:{conf.ZMQ_PORT}")

        script_dir = os.path.dirname(os.path.abspath(__file__))
        ui_file = os.path.join(script_dir, "orchestrator", "zm_ui.ui")
        Form, Window = uic.loadUiType(ui_file)
        
        self.window = Window()
        self.objects_classes = dict()
        self.form = Form()
        self.form.setupUi(self.window)

        # Configuration de l'arbre de données
        self.tree = dict()
        self.tree_lock = threading.Lock()
        self.tree_model = QStandardItemModel()
        self.tree_model.setHorizontalHeaderLabels(['Key', 'Value'])
        self.form.zmView.setModel(self.tree_model)
        self.form.zmView.expandAll()
        
        # Démarrage du thread de réception si pas en mode debug
        if not self.debug_mode:
            self.z_recv_thread = threading.Thread(target=self.recv_thread_func)
            self.z_recv_thread.start()
        else:
            print("Mode debug : pas de connexion au robot")
        
        # Configuration des signaux
        self.tree_update.connect(self.update_tree_view)
        
        # Initialisation des coordonnées cibles
        self.target_coord = [0, 0, 0]
        self.arm_target_pose = [0, 0, 0, 0]

        # Connexion des boutons de contrôle de base
        self.form.minusX.clicked.connect(lambda: self.set_coord(-0.1, 0, 0))
        self.form.minusY.clicked.connect(lambda: self.set_coord(0, -0.1, 0))
        self.form.minusW.clicked.connect(lambda: self.set_coord(0, 0, -0.1))
        self.form.plusX.clicked.connect(lambda: self.set_coord(0.1, 0, 0))
        self.form.plusY.clicked.connect(lambda: self.set_coord(0, 0.1, 0))
        self.form.plusW.clicked.connect(lambda: self.set_coord(0, 0, 0.1))
        self.form.go.clicked.connect(self.send_coord)

        # Connexion des boutons de contrôle du bras
        self.form.armMinusX.clicked.connect(lambda: self.set_arm_coord(-5, 0, 0, 0))
        self.form.armMinusY.clicked.connect(lambda: self.set_arm_coord(0, -5, 0, 0))
        self.form.armMinusZ.clicked.connect(lambda: self.set_arm_coord(0, 0, -5, 0))
        self.form.armMinusRot.clicked.connect(lambda: self.set_arm_coord(0, 0, 0, -5))
        self.form.armPlusX.clicked.connect(lambda: self.set_arm_coord(5, 0, 0, 0))
        self.form.armPlusY.clicked.connect(lambda: self.set_arm_coord(0, 5, 0, 0))
        self.form.armPlusZ.clicked.connect(lambda: self.set_arm_coord(0, 0, 5, 0))
        self.form.armPlusRot.clicked.connect(lambda: self.set_arm_coord(0, 0, 0, 5))
        self.form.armGo.clicked.connect(self.send_arm_coord)
        self.form.alignArmButton.clicked.connect(self.align_arm)

        # Connexion des contrôles principaux
        self.form.stopButton.clicked.connect(self.stop_robot)
        self.form.enableButton.clicked.connect(self.enable_robot)
        self.form.videoButton.clicked.connect(self.toggle_video)
        self.form.inferenceCheckBox.stateChanged.connect(self.toggle_inference)

        # Configuration de la communication ZMQ
        if not self.debug_mode:
            self.pub_socket = self.zmq_context.socket(zmq.PUB)
            try:
                self.pub_socket.bind(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")
            except zmq.error.ZMQError as e:
                print(f"Erreur lors de la liaison du socket : {e}")
                self.debug_mode = True
                print("Passage en mode debug")

        # Initialisation du log
        self.log = list()
        self.log_finished.connect(self.show_log)

        # Configuration de la vidéo
        self.video_state = "uninitialized"
        self.video_capture = None
        self.video_timer = QTimer()
        self.video_timer.timeout.connect(self.update_video_frame)

        # Configuration de l'inférence
        if VISION_AVAILABLE:
            self.inference_module = InferenceModule()
            self.inference_enabled = False
            if not self.inference_module.available:
                self.form.inferenceCheckBox.setEnabled(False)
                self.form.inferenceCheckBox.setToolTip("Vision module is not available")
        else:
            self.inference_module = None
            self.inference_enabled = False
            self.form.inferenceCheckBox.setEnabled(False)
            self.form.inferenceCheckBox.setToolTip("Vision module is not available")

        # Configuration de la séquence
        self.setup_sequence_tab()
        self.sequence_file = os.path.join(os.path.dirname(__file__), "saved_sequence.yaml")
        self.load_sequence()
        # Configuration de la sauvegarde à la fermeture
        self.window.closeEvent = self.custom_close_event
        # Affichage de la fenêtre
        self.window.show()

        # Ajout du combo box pour le type de mouvement du bras
        self.form.armMoveTypeCombo.addItems([
            "Absolute",
            "Base Relative",
            "Tool Relative"
        ])
        self.form.armMoveTypeCombo.setCurrentText("Absolute")  # Valeur par défaut

        # Connecter le changement de type de mouvement
        self.form.armMoveTypeCombo.currentTextChanged.connect(self.on_movement_type_changed)

        # Initialiser les champs avec la position actuelle du bras
        def init_arm_position():
            self.tree_lock.acquire()
            current_position = self.tree.get("/poupette/arm_position", None)
            self.tree_lock.release()
            
            if current_position is not None:
                # Mettre à jour les champs avec la position actuelle
                self.arm_target_pose = [
                    current_position[0],  # x
                    current_position[1],  # y
                    current_position[2],  # z
                    current_position[5],  # yaw (rotation)
                ]
                # Mettre à jour l'interface
                self.form.armXcoord.setText(str(int(self.arm_target_pose[0])))
                self.form.armYcoord.setText(str(int(self.arm_target_pose[1])))
                self.form.armZcoord.setText(str(int(self.arm_target_pose[2])))
                self.form.armRotCoord.setText(str(int(self.arm_target_pose[3])))
                return True
            return False

        # Essayer d'initialiser les positions toutes les 100ms jusqu'à ce que ça réussisse
        self.init_timer = QTimer()
        self.init_timer.timeout.connect(lambda: self.init_timer.stop() if init_arm_position() else None)
        self.init_timer.start(100)

        # Configuration du bouton Align
        self.form.alignArmButton.toggled.connect(self.on_align_arm_toggled)
        self.align_enabled = False
        self.align_timer = QTimer()
        self.align_timer.timeout.connect(self.check_alignment)

        # Constantes et variables pour l'alignement
        self.rotation_aligned = False  # Pour suivre l'état de l'alignement en rotation
        self.rotation_stable_count = 0  # Pour compter combien de fois la rotation est stable
        self.ROTATION_STABILITY_THRESHOLD = 5
        self.MIN_COMMAND_INTERVAL = 0.2  # 200ms entre les commandes
        self.last_arm_command_time = 0  # Pour suivre le timing des commandes

    def set_coord(self, x, y, w):
        self.target_coord[0] += x
        self.target_coord[1] += y
        self.target_coord[2] += w
        self.form.baseXcoord.setText(str(round(self.target_coord[0],3)))
        self.form.baseYcoord.setText(str(round(self.target_coord[1],3)))
        self.form.baseZcoord.setText(str(round(self.target_coord[2],3)))

    def send_coord(self):
        """Envoie les coordonnées de la base"""
        msg = yaml.safe_dump(self.target_coord).encode()
        self.pub_socket.send_multipart([b"/control/poupette/new_position", msg])
        self.target_coord = [0,0,0]
        self.set_coord(0,0,0)
        self.log_thread = threading.Thread(target=self.log_thread_func)
        self.log_thread.start()

    def update_tree_view(self, path: str, value):
        def get_or_create_item(parent_item, name):
            for i in range(parent_item.rowCount()):
                child_item = parent_item.child(i)
                if child_item.text() == name:
                    return i, child_item, False  # False indique que l'item existait déjà
            # Créer une nouvelle ligne avec deux colonnes
            key_item = QStandardItem(name)
            value_item = QStandardItem("")  # Valeur vide initialement
            parent_item.appendRow([key_item, value_item])
            return parent_item.rowCount() - 1, key_item, True  # True indique un nouvel item

        # Start at the root item
        parts = path.strip('/').split('/')
        parent_item = self.tree_model.invisibleRootItem()
        new_item_created = False

        for part in parts[:-1]:  # Navigate through all but the last part
            parent_i, parent_item, is_new = get_or_create_item(parent_item, part)
            new_item_created = new_item_created or is_new
            if is_new:
                # Expand uniquement si c'est un nouvel item
                self.form.zmView.expand(self.tree_model.indexFromItem(parent_item))
                self.form.zmView.resizeColumnToContents(0)
                self.form.zmView.setSortingEnabled(True)
                self.form.zmView.sortByColumn(0, Qt.SortOrder.AscendingOrder)

        last_part = parts[-1]
        last_i, last_item, is_new = get_or_create_item(parent_item, last_part)
        new_item_created = new_item_created or is_new

        # Mettre à jour la valeur dans la deuxième colonne
        value_item = QStandardItem(str(value))
        parent_item.setChild(last_i, 1, value_item)

        if new_item_created:
            # Expand uniquement si un nouvel item a été créé dans le chemin
            self.form.zmView.expand(self.tree_model.indexFromItem(parent_item))


        if path == "/poupette/video_state":
            if not self.debug_mode:
                self.video_state = value
                if value == "fail":
                    self.stop_video()
                    self.form.videoButton.setText("Video Failed")
                    self.form.videoButton.setEnabled(False)

    def log_thread_func(self):
        start_time = time.time()
        self.log=list()
        while time.time()-start_time < 2:
            self.tree_lock.acquire()
            self.log.append(dict(self.tree))
            self.tree_lock.release()
            time.sleep(0.05)
        # self.log_finished.emit()

    def show_log(self):
        pickle.dump(self.log, open("log.pickle", "wb"))
        toshow = [
            "/poupette/fl/current_rpm",
            "/poupette/fr/current_rpm",
            "/poupette/bl/current_rpm",
            "/poupette/br/current_rpm"
        ]
        yseries = list()
        for _ in toshow:
            yseries.append(list())
        for tree in self.log:
            for i,s in enumerate(toshow):
                yseries[i].append(tree.get(s, None))

        fig = plt.figure()
        x = np.arange(len(yseries[0]))*0.05
        for l, ys in zip(toshow, yseries):
            plt.plot(x,ys, label=l)
        plt.show()
        print(yseries[0][:40])
        print(yseries[1][:40])
        print(self.log[:10])
        plt.legend(loc='upper right')


    def recv_thread_func(self):
        if self.debug_mode:
            return

        s = self.zmq_context.socket(zmq.SUB)
        s.connect(f"tcp://192.168.2.2:{conf.ZMQ_PORT}")
        s.connect(f"tcp://{conf.Poupette.external_ip}:{conf.ZMQ_PORT}")
        s.connect(f"tcp://{conf.Bichon.external_ip}:{conf.ZMQ_PORT}")
        s.connect(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")
        
        s.setsockopt(zmq.SUBSCRIBE, b'')
        
        print("Started thread")
        while True:
            try:
                topic, msg = s.recv_multipart()
                arr = yaml.safe_load(msg.decode('utf-8'))
                topic = topic.decode('utf-8')
                self.tree_lock.acquire()
                self.tree[topic] = arr
                self.tree_lock.release()
                self.tree_update.emit(topic, str(arr))
            except zmq.error.ContextTerminated:
                break

    def stop_robot(self):
        print("Stopping robot")
        self.set_robot_enable(0)

    def enable_robot(self):
        print("Enabling robot")
        self.set_robot_enable(1)

    def set_robot_enable(self, enable):
        enable_command = yaml.safe_dump(enable).encode()
        self.pub_socket.send_multipart([b"/control/poupette/enable", enable_command])
        
        # Réinitialiser les coordonnées cibles
        self.target_coord = [0, 0, 0]
        self.set_coord(0, 0, 0)

    def check_ip_and_set_mode(self):
        try:
            # Obtenir l'adresse IP de l'interface principale
            gateways = netifaces.gateways()
            default_interface = gateways['default'][netifaces.AF_INET][1]
            addrs = netifaces.ifaddresses(default_interface)
            current_ip = addrs[netifaces.AF_INET][0]['addr']

            print(f"Adresse IP actuelle : {current_ip}")
            print(f"Adresse IP de l'orchestrateur : {conf.Orchestrator.external_ip}")

            if current_ip != conf.Orchestrator.external_ip:
                print("L'adresse IP actuelle ne correspond pas à celle de l'orchestrateur.")
                self.debug_mode = True
                print("Passage en mode debug")
            else:
                print("L'adresse IP correspond à celle de l'orchestrateur.")
        except Exception as e:
            print(f"Erreur lors de la vérification de l'adresse IP : {e}")
            self.debug_mode = True
            print("Passage en mode debug")

    def toggle_video(self):
        if self.video_state == "uninitialized":
            self.start_video()
        elif self.video_state == "emitting":
            self.stop_video()

    def start_video(self):
        if not self.debug_mode:
            self.pub_socket.send_multipart([b"/control/poupette/video", b"start"])
            
            # Set up v4l2loopback
            subprocess.run(["sudo", "rmmod", "v4l2loopback"], check=False)
            subprocess.run(["sudo", "modprobe", "v4l2loopback", "video_nr=32", "card_label='Virtual Camera'"], check=True)
            
            # Start netcat to receive video and pipe it to ffmpeg
            nc_cmd = f"nc -l {conf.Poupette.video_port} | ffmpeg -f mjpeg -i - -vcodec rawvideo -pix_fmt rgb24 -f v4l2 /dev/video32"
            self.nc_process = subprocess.Popen(nc_cmd, shell=True)
            
            # Wait a bit for the virtual device to be ready
            time.sleep(1)
            
            # Open the virtual video device
            self.video_capture = cv2.VideoCapture("/dev/video32")
        else:
            # Debug mode: use local camera
            self.video_capture = cv2.VideoCapture(0)
        
        if self.video_capture.isOpened():
            self.form.videoButton.setText("Stop Video")
            self.video_timer.start(33)  # ~30 fps
            self.video_state = "emitting"
        else:
            print("Unable to open camera")
            self.video_state = "fail"
            self.form.videoButton.setText("Video Failed (retry)")

    def stop_video(self):
        if not self.debug_mode:
            self.pub_socket.send_multipart([b"/control/poupette/video", b"stop"])
            if hasattr(self, 'nc_process'):
                self.nc_process.terminate()
            # Remove the v4l2loopback device
            subprocess.run(["sudo", "rmmod", "v4l2loopback"], check=False)
        
        self.video_timer.stop()
        if self.video_capture:
            self.video_capture.release()
            self.video_capture = None
        
        self.form.videoButton.setText("Start Video")
        self.video_state = "uninitialized"

    def toggle_inference(self, state):
        if not VISION_AVAILABLE or not self.inference_module.available:
            return
        
        self.inference_enabled = state == Qt.Checked
        if self.inference_enabled:
            # Charger le modèle par défaut
            default_model_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "vision", "models", "exp167.pt"))
            if not self.inference_module.load_model(default_model_path):
                self.inference_enabled = False
                self.form.inferenceCheckBox.setChecked(False)

    def update_video_frame(self):
        if self.video_capture:
            ret, frame = self.video_capture.read()
            if ret:
                if self.inference_enabled:
                    height, width = frame.shape[:2]
                    image_center = (width // 2, height // 2)
                    
                    # Obtenir les dimensions de l'image
                    height, width = frame.shape[:2]
                    image_center = (width // 2, height // 2)
                    
                    # Effectuer l'inférence sur l'image
                    annotations = self.inference_module.run_inference(frame)
                    
                    # Trier les annotations par indice de confiance décroissant et prendre les deux premières
                    sorted_annotations = sorted(annotations, key=lambda x: x[4], reverse=True)[:2]
                    
                    centers = []
                    for ann in sorted_annotations:
                        x1, y1, x2, y2, conf, cls = ann
                        class_name = self.inference_module.model.names[int(cls)]
                        cv2.rectangle(frame, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
                        cv2.putText(frame, f"{class_name} {conf:.2f}", (int(x1), int(y1) - 10),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                        
                        # Calculer le centre de la boîte englobante
                        center_x = int((x1 + x2) / 2)
                        center_y = int((y1 + y2) / 2)
                        centers.append((center_x, center_y))
                    
                    # Si deux annotations sont détectées
                    if len(centers) == 2:
                        x1, y1 = centers[0]
                        x2, y2 = centers[1]
                        dx = x2 - x1
                        dy = y2 - y1
                        if abs(dx) > abs(dy):
                            if x1>x2:
                                x1, x2 = x2, x1
                                y1, y2 = y2, y1
                        else:
                            if y1>y2:
                                x1, x2 = x2, x1
                                y1, y2 = y2, y1
                        
                        angle = math.atan2((y2 - y1), (x2 - x1))
                        angle_degrees = math.degrees(angle)
                        
                        # Calculer les points d'intersection avec les bords de l'image
                        if x2 != x1:  # Ligne non verticale
                            m = (y2 - y1) / (x2 - x1)
                            b = y1 - m * x1
                            
                            # Points d'intersection avec les bords verticaux
                            y_left = m * 0 + b
                            y_right = m * width + b
                            
                            # Points d'intersection avec les bords horizontaux
                            x_top = (0 - b) / m if m != 0 else x1
                            x_bottom = (height - b) / m if m != 0 else x1
                            
                            # Collecter tous les points d'intersection valides
                            points = []
                            if 0 <= y_left <= height:
                                points.append((0, int(y_left)))
                            if 0 <= y_right <= height:
                                points.append((width, int(y_right)))
                            if 0 <= x_top <= width:
                                points.append((int(x_top), 0))
                            if 0 <= x_bottom <= width:
                                points.append((int(x_bottom), height))
                            
                            # Prendre les deux points les plus éloignés
                            if len(points) >= 2:
                                points = sorted(points, key=lambda p: (p[0] - x1)**2 + (p[1] - y1)**2)
                                cv2.line(frame, points[0], points[-1], (0, 0, 255), 2)
                        else:  # Ligne verticale
                            cv2.line(frame, (x1, 0), (x1, height), (0, 0, 255), 2)
                        
                        # Calculer l'offset comme la distance perpendiculaire du centre à la ligne
                        # Formule: |ax0 + by0 + c|/sqrt(a² + b²) où ax + by + c = 0 est l'équation de la ligne
                        a = y2 - y1
                        b = x1 - x2
                        c = x2*y1 - x1*y2
                        
                        # Distance signée du centre à la ligne
                        offset = (a*image_center[0] + b*image_center[1] + c) / math.sqrt(a*a + b*b)
                        
                        # Publier l'angle et l'offset si nous ne sommes pas en mode debug
                        if not self.debug_mode:
                            self.pub_socket.send_multipart([b"/vision/angle", yaml.safe_dump(angle_degrees).encode()])
                            self.pub_socket.send_multipart([b"/vision/offset", yaml.safe_dump(int(offset)).encode()])
                        


                rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                h, w, ch = rgb_image.shape
                bytes_per_line = ch * w
                qt_image = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
                self.form.videoLabel.setPixmap(QPixmap.fromImage(qt_image))
            else:
                print("Error capturing video frame")
                self.stop_video()

    def set_arm_coord(self, x, y, z, rot):
        """Met à jour les coordonnées cibles du bras (en relatif pour l'affichage)"""
        self.arm_target_pose[0] += x
        self.arm_target_pose[1] += y
        self.arm_target_pose[2] += z
        self.arm_target_pose[3] += rot
        self.form.armXcoord.setText(str(int(self.arm_target_pose[0])))
        self.form.armYcoord.setText(str(int(self.arm_target_pose[1])))
        self.form.armZcoord.setText(str(int(self.arm_target_pose[2])))
        self.form.armRotCoord.setText(str(int(self.arm_target_pose[3])))

    def send_arm_coord(self):
        """Envoie la pose du bras au robot avec le type de référentiel"""
        movement_type = self.form.armMoveTypeCombo.currentText().lower().replace(' ', '_')
        
        arm_command = [
            float(self.form.armXcoord.text()),
            float(self.form.armYcoord.text()),
            float(self.form.armZcoord.text()),
            float(self.form.armRotCoord.text()),
            movement_type
        ]
        
        msg = yaml.safe_dump(arm_command).encode()
        print(f"Sending arm command: {arm_command}")
        self.pub_socket.send_multipart([b"/control/poupette/arm_pose", msg])
        
        # Réinitialiser l'affichage des coordonnées relatives si ce n'est pas en mode absolu
        if movement_type != 'absolute':
            self.arm_target_pose = [0, 0, 0, 0]
            self.set_arm_coord(0, 0, 0, 0)

    def setup_sequence_tab(self):
        """Configure le tab de séquence d'actions"""
        self.form.addActionButton.clicked.connect(self.add_action)
        self.form.deleteActionButton.clicked.connect(self.delete_action)
        self.form.playSequenceButton.clicked.connect(self.play_sequence)
        
        # Menu contextuel pour l'ajout d'actions
        self.action_menu = QMenu()
        self.action_menu.addAction("Move Base", lambda: self.add_specific_action("move_base"))
        self.action_menu.addAction("Move Arm", lambda: self.add_specific_action("move_arm"))
        self.action_menu.addAction("Open Gripper", lambda: self.add_specific_action("open_gripper"))
        self.action_menu.addAction("Close Gripper", lambda: self.add_specific_action("close_gripper"))
        self.action_menu.addAction("Auto", lambda: self.add_specific_action("auto"))

    def add_action(self):
        """Affiche le menu pour ajouter une action"""
        self.action_menu.exec_(QCursor.pos())

    def add_specific_action(self, action_type):
        """Ajoute une action spécifique à la liste"""
        if action_type == "move_base":
            item_text = f"Move Base ({self.target_coord[0]}, {self.target_coord[1]}, {self.target_coord[2]})"
            params = self.target_coord.copy()
        elif action_type == "move_arm":
            item_text = f"Move Arm ({self.arm_target_pose[0]}, {self.arm_target_pose[1]}, {self.arm_target_pose[2]}, {self.arm_target_pose[3]})"
            params = self.arm_target_pose.copy()
        elif action_type == "open_gripper":
            item_text = "Open Gripper"
            params = True
        elif action_type == "close_gripper":
            item_text = "Close Gripper"
            params = False
        else:  # auto
            item_text = "Auto"
            params = None
        
        item = QListWidgetItem(item_text)
        item.setData(Qt.UserRole, {
            "type": action_type,
            "params": params
        })
        self.form.sequenceList.addItem(item)

    def delete_action(self):
        """Supprime l'action sélectionnée"""
        current_item = self.form.sequenceList.currentItem()
        if current_item:
            self.form.sequenceList.takeItem(self.form.sequenceList.row(current_item))

    def play_sequence(self):
        """Exécute la séquence d'actions"""
        try:
            sequence_length = self.form.sequenceList.count()
            print(f"Starting sequence execution - {sequence_length} actions found")
            
            # Style pour highlight
            default_bg = self.form.sequenceList.item(0).background() if sequence_length > 0 else None
            highlight_color = QColor(255, 255, 0, 100)
            
            for i in range(sequence_length):
                item = self.form.sequenceList.item(i)
                action_data = item.data(Qt.UserRole)
                action_type = action_data["type"]
                params = action_data["params"]
                
                item.setBackground(QBrush(highlight_color))
                self.form.sequenceList.scrollToItem(item)
                QApplication.processEvents()
                
                print(f"Executing action {i+1}/{sequence_length}: {action_type} with params {params}")
                
                if action_type == "move_base":
                    print(f"-> Sending move base command to {params}")
                    msg = yaml.safe_dump(params).encode()
                    self.pub_socket.send_multipart([b"/control/poupette/new_position", msg])
                    time.sleep(0.1)
                    # Attendre que le mouvement soit terminé en vérifiant tree
                    while True:
                        self.tree_lock.acquire()
                        ticks = self.tree.get("/poupette/base/ticks_to_destination", float('inf'))
                        self.tree_lock.release()
                        
                        if ticks < 20:
                            print("Base movement completed")
                            break
                        QApplication.processEvents()
                        time.sleep(0.01)
                
                elif action_type == "move_arm":
                    print(f"-> Sending move arm command to {params}")
                    msg = yaml.safe_dump(params).encode()
                    self.pub_socket.send_multipart([b"/control/poupette/arm_pose", msg])
                    time.sleep(2)  # Attente fixe pour le moment
                
                elif action_type == "open_gripper":
                    print("-> Sending open gripper command")
                    msg = yaml.safe_dump("OPEN").encode()
                    self.pub_socket.send_multipart([b"/poupette/gripper/command", msg])
                    time.sleep(1)
                
                elif action_type == "close_gripper":
                    print("-> Sending close gripper command")
                    msg = yaml.safe_dump("CLOSE").encode()
                    self.pub_socket.send_multipart([b"/poupette/gripper/command", msg])
                    time.sleep(1)
                
                elif action_type == "auto":
                    print("-> Sending auto sequence command")
                    msg = yaml.safe_dump("auto").encode()
                    self.pub_socket.send_multipart([b"/control/poupette/auto", msg])
                    time.sleep(1)
                
                # Restaurer la couleur de fond normale
                item.setBackground(default_bg)
                
            print("Sequence execution completed")
            
        except Exception as e:
            print(f"Error during sequence execution: {str(e)}")
            traceback.print_exc()

    def custom_close_event(self, event):
        """Gère la fermeture de l'application"""
        try:
            # Arrêter la vidéo et le timer
            self.video_timer.stop()
            if self.video_state != "uninitialized":
                self.stop_video()
            
            # Sauvegarder la séquence
            self.save_sequence()
            
            # Fermer proprement les sockets ZMQ
            if hasattr(self, 'pub_socket'):
                self.pub_socket.close()
            
            # Arrêter le contexte ZMQ et le thread de réception
            if hasattr(self, 'zmq_context'):
                self.zmq_context.term()
                if hasattr(self, 'z_recv_thread'):
                    self.z_recv_thread.join(timeout=1)  # Attendre max 1 seconde
            
            # Accepter l'événement de fermeture
            event.accept()
            
        except Exception as e:
            print(f"Error during closing: {e}")
            event.accept()

    def save_sequence(self):
        """Sauvegarde la séquence d'actions dans un fichier"""
        sequence = []
        for i in range(self.form.sequenceList.count()):
            item = self.form.sequenceList.item(i)
            action_data = item.data(Qt.UserRole)
            sequence.append({
                'text': item.text(),
                'action_type': action_data['type'],
                'params': action_data['params']
            })
        
        try:
            with open(self.sequence_file, 'w') as f:
                yaml.safe_dump(sequence, f)
            print(f"Sequence saved to {self.sequence_file}")
        except Exception as e:
            print(f"Error saving sequence: {e}")

    def load_sequence(self):
        """Charge la séquence d'actions depuis le fichier"""
        if not os.path.exists(self.sequence_file):
            print("No saved sequence found")
            return
            
        try:
            with open(self.sequence_file, 'r') as f:
                sequence = yaml.safe_load(f)
                
            if sequence:
                for action in sequence:
                    item = QListWidgetItem(action['text'])
                    item.setData(Qt.UserRole, {
                        'type': action['action_type'],
                        'params': action['params']
                    })
                    self.form.sequenceList.addItem(item)
                print(f"Sequence loaded from {self.sequence_file}")
        except Exception as e:
            print(f"Error loading sequence: {e}")

    def align_arm(self):
        """Aligne le bras avec la ligne détectée en utilisant la position absolue"""
        self.tree_lock.acquire()
        current_angle = self.tree.get("/vision/angle", None)
        current_position = self.tree.get("/poupette/arm_position", None)
        self.tree_lock.release()
        
        if current_angle is not None and current_position is not None:
            # Calculer la rotation nécessaire pour atteindre 90 degrés
            rotation_needed = 90 - current_angle
            
            # Créer une nouvelle pose absolue en conservant les positions x,y,z actuelles
            # et en ajoutant la rotation nécessaire à la rotation actuelle
            absolute_pose = [
                current_position[0],
                current_position[1],
                current_position[2],
                current_position[5] + rotation_needed  # Ajouter à la rotation actuelle
            ]
            
            # Envoyer la pose absolue
            msg = yaml.safe_dump(absolute_pose).encode()
            self.pub_socket.send_multipart([b"/control/poupette/arm_pose", msg])
            print(f"Sending arm pose command with absolute rotation: {absolute_pose[3]} degrees")
            
            # Réinitialiser la pose cible relative
            self.arm_target_pose = [0, 0, 0, 0]
        else:
            print("No line detected to align with or current arm position not available")

    def on_movement_type_changed(self, movement_type):
        """Gère le changement de type de mouvement du bras"""
        if movement_type.lower() == "absolute":
            # Récupérer la position actuelle du bras depuis l'arbre de données
            self.tree_lock.acquire()
            current_position = self.tree.get("/poupette/arm_position", None)
            self.tree_lock.release()
            
            if current_position is not None:
                # Mettre à jour les champs avec la position actuelle
                self.arm_target_pose = [
                    current_position[0],  # x
                    current_position[1],  # y
                    current_position[2],  # z
                    current_position[5],  # yaw (rotation)
                ]
                # Mettre à jour l'interface
                self.form.armXcoord.setText(str(int(self.arm_target_pose[0])))
                self.form.armYcoord.setText(str(int(self.arm_target_pose[1])))
                self.form.armZcoord.setText(str(int(self.arm_target_pose[2])))
                self.form.armRotCoord.setText(str(int(self.arm_target_pose[3])))
            else:
                print("Warning: Could not get current arm position")
        else:
            # En mode relatif, réinitialiser à zéro
            self.arm_target_pose = [0, 0, 0, 0]
            self.form.armXcoord.setText("0")
            self.form.armYcoord.setText("0")
            self.form.armZcoord.setText("0")
            self.form.armRotCoord.setText("0")

    def on_align_arm_toggled(self, checked):
        """Gère l'activation/désactivation de l'alignement automatique"""
        self.align_enabled = checked
        self.rotation_aligned = False  # Réinitialiser l'état d'alignement
        self.rotation_stable_count = 0
        
        if checked:
            self.form.alignStatusLabel.setText("Starting alignment")
            self.align_timer.start(100)
        else:
            self.form.alignStatusLabel.setText("Alignment disabled")
            self.align_timer.stop()

    def check_alignment(self):
        """Vérifie et ajuste l'alignement en deux phases : rotation puis translation"""
        if not self.align_enabled:
            return
            
        # Vérifier le délai depuis la dernière commande
        current_time = time.time()
        if current_time - self.last_arm_command_time < self.MIN_COMMAND_INTERVAL:
            return
            
        self.tree_lock.acquire()
        current_angle = self.tree.get("/vision/angle", None)
        current_position = self.tree.get("/poupette/arm_position", None)
        current_offset = self.tree.get("/vision/offset", None)
        arm_moving = not self.tree.get("/poupette/arm_position_reached", False)
        self.tree_lock.release()

        # Préparer le message de statut avec les erreurs
        angle_error = 90 - current_angle if current_angle is not None else "N/A"
        offset_error = current_offset if current_offset is not None else "N/A"
        status_prefix = f"a:{angle_error:.1f}° o:{offset_error:.1f}px - "

        # Ne rien faire si le bras est en mouvement
        if arm_moving:
            self.form.alignStatusLabel.setText(f"{status_prefix}Waiting for movement to complete...")
            return
        
        if current_angle is not None and current_position is not None and current_offset is not None:
            # Phase 1 : Correction de la rotation
            if not self.rotation_aligned and self.align_enabled:
                rotation_needed = 90 - current_angle
                
                if abs(rotation_needed) > 2:
                    self.rotation_stable_count = 0
                    self.form.alignStatusLabel.setText(f"{status_prefix}Rotating {rotation_needed:.1f}°")
                    
                    if self.align_enabled:
                        absolute_pose = [
                            current_position[0],
                            current_position[1],
                            current_position[2],
                            current_position[5] + rotation_needed
                        ]
                        
                        msg = yaml.safe_dump(absolute_pose).encode()
                        self.pub_socket.send_multipart([b"/control/poupette/arm_pose", msg])
                        self.last_arm_command_time = current_time  # Mettre à jour le temps de la dernière commande
                        return
                else:
                    self.rotation_stable_count += 1
                    self.form.alignStatusLabel.setText(
                        f"{status_prefix}Stabilizing rotation ({self.rotation_stable_count}/{self.ROTATION_STABILITY_THRESHOLD})"
                    )
                    
                    if self.rotation_stable_count >= self.ROTATION_STABILITY_THRESHOLD:
                        self.rotation_aligned = True
                        self.form.alignStatusLabel.setText(f"{status_prefix}Rotation aligned, correcting offset")
            
            # Phase 2 : Correction de l'offset en Y
            elif self.rotation_aligned and self.align_enabled:
                PIXEL_TO_MM = -0.2
                y_correction = current_offset * PIXEL_TO_MM
                print(f"y_correction: {y_correction} align_enabled: {self.align_enabled}")
                if abs(y_correction) > 2:
                    self.form.alignStatusLabel.setText(f"{status_prefix}Correcting Y offset: {y_correction:.1f}mm")
                    
                    if self.align_enabled:
                        tool_relative_command = [
                            0,
                            y_correction,
                            0,
                            0,
                            'tool_relative'
                        ]
                        
                        msg = yaml.safe_dump(tool_relative_command).encode()
                        self.pub_socket.send_multipart([b"/control/poupette/arm_pose", msg])
                        self.last_arm_command_time = current_time  # Mettre à jour le temps de la dernière commande
                        return
                else:
                    self.form.alignStatusLabel.setText(f"{status_prefix}Fully aligned")
                    self.align_enabled = False
                    self.form.alignArmButton.setChecked(False)

if __name__ == "__main__":
    app = App()
    app.window.show()
    sys.exit(app.exec_())



