#!/usr/bin/env python
# coding: utf-8

"""
Gripper Control for Robot

Features:
1. Communication with the main PC via ZeroMQ.
2. Control of a gripper via serial port (Arduino).
3. Reception of joystick commands to adjust the gripper's grip level.
4. Publication of the gripper's grip level.
5. Management of video streaming from the gripper camera.

Constraints:
1. Use of Python with ZeroMQ and pyserial.
2. Real-time communication with Arduino.
3. Integration into the existing robot topic hierarchy.
4. Automatic selection of the first device in /dev/serial/by-id/
5. Implements error handling and reconnection attempts for serial communication.
6. Video streaming using v4l2 and netcat.

Code structure:
- GripperController class: Manages communication, control of the gripper, and video streaming.

Important notes:
- The code uses ZeroMQ for communication with other parts of the system.
- Serial communication is used to control the gripper via Arduino.
- Video streaming is managed using subprocess to run v4l2 and netcat commands.
- The system can start/stop video streaming based on received commands.

TODO / Possible improvements:
- Implement more robust error handling for video streaming.
- Add configuration options for video quality and frame rate.
- Implement a heartbeat mechanism to ensure continuous communication with the main system.
- Add logging for better debugging and monitoring.
"""

import zmq
import serial
import time
import yaml
import os
import glob
import subprocess
from mual import conf
from mual.control.node import Node
import sys



class GripperController(Node):
    def __init__(self, name, robot_conf):
        super().__init__(name, zmq_port=conf.ZMQ_PORT+1)
        
        self.grip_level_open = 130  # Grip level for open state
        self.grip_level_closed = 70  # Grip level for closed state
        self.grip_level = self.grip_level_open  # Initialize to open
        self.last_sent_grip_level = self.grip_level  # To track the last sent level
        self.is_closed = False  # New attribute to track gripper state
        self.last_button_state = False  # To track button state changes
        self.robot_conf = robot_conf

        self.serial = self.create_serial_connection()
        if not self.serial:
            raise Exception("Failed to initialize serial connection")
        
        # ZMQ Configuration
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)
        self.socket.connect(f"tcp://{self.robot_conf.external_ip}:{conf.ZMQ_PORT}")
        self.socket.connect(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")
        self.socket.setsockopt_string(zmq.SUBSCRIBE, "/"+robot_name+"/joy/joy_data")
        self.socket.setsockopt_string(zmq.SUBSCRIBE, "/"+robot_name+"/gripper/command")
        self.socket.setsockopt_string(zmq.SUBSCRIBE, "/control/"+robot_name+"/video")
        print(f"Subscribed to {robot_name} ({self.robot_conf.external_ip}) and Orchestrator ({conf.Orchestrator.external_ip})")

        # Ajout des attributs pour la gestion vidéo
        self.video_process = None
        self.video_state = "uninitialized"
        self.video_port = self.robot_conf.video_port  # Assurez-vous que cette configuration existe
    
    def get_serial_port(self):
        specific_device = self.robot_conf.specific_device
        if os.path.exists(specific_device):
            return os.path.realpath(specific_device)
        return None

    def create_serial_connection(self):
        while True:
            port = self.get_serial_port()
            if port:
                try:
                    ser = serial.Serial(port, 9600, timeout=1)
                    print(f"Connected to {port}")
                    return ser
                except serial.SerialException as e:
                    print(f"Failed to connect to the serial port: {e}")
            else:
                print("Device not found. Retrying in 3 seconds...")
            
            time.sleep(3)  # Wait for 3 seconds before retrying

    def send_command(self, command):
        try:
           
            # Send the command
            self.serial.write(command.encode())
            self.serial.flush()
            
            # Wait a bit
            time.sleep(0.05)
            
        except serial.SerialException as e:
            print(f"Error sending command: {e}")
            self.reconnect_serial()

    def reconnect_serial(self):
        print("Attempting to reconnect...")
        if self.serial:
            self.serial.close()
        self.serial = create_serial_connection()

    def update_gripper(self):
        self.send_command(f"111 {self.grip_level} 222\n")
        self.last_sent_grip_level = self.grip_level
        self.publish("gripper/grip_level", self.grip_level)

    def process_joy_data(self, joy_data):
        buttons = joy_data["buttons"]
        axes = joy_data["axes"]
        
        new_grip_level = self.grip_level
        trigger_value = axes[5]

        # Toggle gripper state when button 2 is pressed
        if buttons[2] == 1 and not self.last_button_state:
            self.is_closed = not self.is_closed
            new_grip_level = self.grip_level_closed if self.is_closed else self.grip_level_open

        # Direct control with trigger when it's pressed more than -0.9
        elif trigger_value > -0.9:
            # Map trigger value from [-0.9, 1] to [grip_level_open, grip_level_closed]
            new_grip_level = int(self.grip_level_open + (trigger_value + 0.9) / 1.9 * (self.grip_level_closed - self.grip_level_open))

        if new_grip_level != self.grip_level:
            self.grip_level = new_grip_level
            self.update_gripper()  # Update the gripper only if the level has changed

       
        # Update button state
        self.last_button_state = buttons[2]

    def start_video_stream(self):
        try:
            # Set camera parameters
            subprocess.run(["v4l2-ctl", "-d", "/dev/video0", "-c", "power_line_frequency=1"], check=True)
            subprocess.run(["v4l2-ctl", "-d", "/dev/video0", "-c", "focus_auto=0"], check=True)
            subprocess.run(["v4l2-ctl", "-d", "/dev/video0", "-c", "focus_absolute=40"], check=True)
            
            # Start video stream
            cmd = [
                "v4l2-ctl", "-d", "/dev/video0", 
                "-v", "width=640,height=480,pixelformat=MJPG", 
                "--stream-mmap", "--stream-to=-", 
                "|", "nc", conf.Orchestrator.external_ip, str(self.video_port)
            ]
            print(f"Starting video stream with command: {' '.join(cmd)}")
            self.video_process = subprocess.Popen(" ".join(cmd), shell=True)
            self.video_state = "emitting"
            print("Video stream started successfully")
        except subprocess.CalledProcessError as e:
            print(f"Error setting camera parameters: {e}")
            self.video_state = "fail"
        except Exception as e:
            print(f"Error starting video stream: {e}")
            self.video_state = "fail"

    def stop_video_stream(self):
        if self.video_process:
            self.video_process.terminate()
            self.video_process = None
        self.video_state = "uninitialized"

    def run(self):
        while True:
            try:
                while True:
                    try:
                        topic, msg = self.socket.recv_multipart(flags=zmq.NOBLOCK)
                        if topic == b"/control/poupette/video":
                            video_command = msg.decode()
                            print(f"Received video command: {video_command}")
                            if video_command == "start" and self.video_state == "uninitialized":
                                self.start_video_stream()
                            elif video_command == "stop" and self.video_state == "emitting":
                                self.stop_video_stream()
                        elif topic == b"/"+bytes((robot_name), encoding='utf8')+b"/gripper/command":
                            command = yaml.safe_load(msg.decode())
                            print(f"Received gripper command: {command}")
                            if command == "OPEN":
                                self.grip_level = self.grip_level_open
                                self.update_gripper()
                            elif command == "CLOSE":
                                self.grip_level = self.grip_level_closed
                                self.update_gripper()
                            elif command.startswith("GRIP_LEVEL"):
                                level = int(command.split()[1])
                                self.grip_level = level
                                self.update_gripper()
                    except zmq.Again:
                        break

                if 'joy_data' in locals():
                    self.process_joy_data(joy_data)
                    del joy_data

                self.publish("video_state", self.video_state)

            except yaml.YAMLError as e:
                print(f"YAML decoding error: {e}")
            except serial.SerialException as e:
                print(f"Serial communication error: {e}")
                self.reconnect_serial()
            except Exception as e:
                print(f"Unexpected error: {e}")

            time.sleep(0.01)

    def __del__(self):
        if self.serial:
            self.serial.close()
        if self.video_process:
            self.video_process.terminate()

if __name__ == "__main__":
    try:
        gripper = GripperController("poupette")
        gripper.run()
    except Exception as e:
        print(f"Failed to initialize GripperController: {e}")
