#!/usr/bin/env python

import sys
import yaml
import zmq
import curses
from . import conf
from time import sleep, time

def draw_tree(stdscr, tree):
    stdscr.clear()
    y = 0
    for k, v in sorted(tree.items()):
        stdscr.addstr(y, 0, f"{k} : {v}")
        y += 1
    stdscr.refresh()

def main(stdscr):
    tree = dict()

    ctx = zmq.Context()
    spub = ctx.socket(zmq.PUB)
    spub.bind(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")


    s = ctx.socket(zmq.SUB)
    s.connect(f"tcp://{conf.Poupette.external_ip}:{conf.ZMQ_PORT}")
    s.connect(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")
    s.setsockopt(zmq.SUBSCRIBE, b'')

    try:
        ts = time()
        iter = 0
        while True:
            mmm = s.recv_multipart()
            topic, msg = mmm
            arr = yaml.safe_load(msg.decode('utf-8'))
            tree[topic] = arr
            if time()-ts > 0.1:
                draw_tree(stdscr, tree)
                ts = time()
                if iter==10:
                    spub.send_multipart([b"/control/poupette/new_position", str([0.5, 0, 0]).encode()])
                spub.send_multipart([b"/iter", str(iter).encode()])
                iter += 1
    except KeyboardInterrupt:
        pass
    stdscr.addstr(len(tree) + 1, 0, "Done.")
    stdscr.refresh()
    stdscr.getch()

if __name__ == "__main__":
    curses.wrapper(main)
