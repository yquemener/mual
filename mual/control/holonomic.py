
    

def inverse_kinematics(vx, vy, wz, lx=.29, ly=.295, r=.08):
    wfl = 1/r*(vx-vy-(lx+ly)*wz)
    wfr = 1/r*(vx+vy+(lx+ly)*wz)
    wrl = 1/r*(vx+vy-(lx+ly)*wz)
    wrr = 1/r*(vx-vy+(lx+ly)*wz)
    return wfl, wfr, wrl, wrr
