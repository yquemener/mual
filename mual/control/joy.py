"""
Joystick Control Node

Fonctionnalités :
1. Lecture de toutes les entrées du joystick (axes et boutons).
2. Publication des données brutes du joystick en tant que Node enfant.
3. Possibilité d'arrêter le nœud proprement.

Contraintes :
1. Utilisation de PyGame pour la lecture du joystick.
2. Intégration avec le système de Node pour la communication.

Structure :
- Classe JoyNode : Gère la lecture du joystick et la publication des données brutes.
"""

import pygame
from mual.control.node import Node
import time

class JoyNode(Node):
    def __init__(self, name, parent):
        super().__init__(name, parent=parent)
        pygame.init()
        self.joy = pygame.joystick.Joystick(0)
        self.joy.init()
        self.running = True

    def get_joy(self):
        pygame.event.get()
        axes = [self.joy.get_axis(i) for i in range(self.joy.get_numaxes())]
        buttons = [self.joy.get_button(i) for i in range(self.joy.get_numbuttons())]
        return {"axes": axes, "buttons": buttons}

    def run(self):
        while self.running:
            joy_data = self.get_joy()
            self.publish("joy_data", joy_data)  # Envoi des données brutes
            time.sleep(0.01)  # Publier à 100Hz

    def stop(self):
        self.running = False

if __name__ == "__main__":
    joy_node = JoyNode("joy", parent=None)
    joy_node.run()
