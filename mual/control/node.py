"""
Node and Publishing System for Robot Control

Fonctionnalités :
1. Système de communication basé sur ZeroMQ pour les nodes.
2. Mécanisme de publication/souscription pour les messages entre nodes.
3. Gestion des threads pour la publication asynchrone des messages.
4. Gestion du port ZMQ à partir de la configuration ou du parent.
5. Gestion centralisée de l'arbre de données pour tous les nœuds.

Contraintes :
1. Utilisation de ZeroMQ pour la communication.
2. Gestion thread-safe des queues de messages.
3. Compatibilité avec le système existant de contrôle du robot.
4. Le port ZMQ est obligatoire pour le nœud racine et hérité par les nœuds enfants.
5. Un seul arbre de données partagé entre tous les nœuds d'une même hiérarchie.
6. Accès thread-safe à l'arbre de données.

Structure :
- Classe Node : Base pour tous les nodes du système.
- Fonction publishing_thread : Gère la publication asynchrone des messages.
- Fonction receiving_thread : Gère la réception et la mise à jour de l'arbre.
"""

import threading
import zmq
import yaml
from queue import Queue
from typing import Optional, Dict, Any

zmq_context = zmq.Context()

def publishing_thread(parent_node):
    socket = parent_node.z_socket
    queue = parent_node.publishing_queue
    while not parent_node.kill_thread:
        tree = {}
        while not queue.empty():
            topic, msg = queue.get()
            tree[topic] = msg
        for msg, val in tree.items():
            socket.send_multipart([msg, val])
    return

def receiving_thread(node):
    socket = node.z_sub_socket
    while not node.kill_thread:
        try:
            topic, msg = socket.recv_multipart()
            data = yaml.safe_load(msg.decode('utf-8'))
            topic = topic.decode('utf-8')
            with node.tree_lock:
                node.tree[topic] = data
            # Notify subscribers if any
            if hasattr(node, 'tree_update_callback'):
                node.tree_update_callback(topic, data)
        except zmq.error.ContextTerminated:
            break

class Node:
    def __init__(self, name: str, parent: Optional['Node'] = None, zmq_port: Optional[int] = None):
        self.name = name
        self.parent = parent
        
        # Initialize tree-related attributes
        if parent is None:
            # Root node initialization
            if zmq_port is None:
                raise ValueError("ZMQ port must be specified for root node")
                
            self.tree: Dict[str, Any] = {}
            self.tree_lock = threading.Lock()
            self.z_path = "/" + self.name + "/"
            self.z_ctx = zmq_context
            
            # Publisher setup
            self.z_socket = self.z_ctx.socket(zmq.PUB)
            self.zmq_port = zmq_port
            self.z_socket.bind(f"tcp://0.0.0.0:{self.zmq_port}")
            
            # Subscriber setup
            self.z_sub_socket = self.z_ctx.socket(zmq.SUB)
            self.z_sub_socket.connect(f"tcp://0.0.0.0:{self.zmq_port}")
            self.z_sub_socket.setsockopt(zmq.SUBSCRIBE, b'')
            
            # Thread management
            self.publishing_queue = Queue()
            self.kill_thread = False
            self.z_pub_thread = threading.Thread(target=publishing_thread, args=(self,))
            self.z_recv_thread = threading.Thread(target=receiving_thread, args=(self,))
            self.z_pub_thread.start()
            self.z_recv_thread.start()
        else:
            # Child node initialization - share parent's resources
            self.z_path = self.parent.z_path + self.name + "/"
            self.z_ctx = parent.z_ctx
            self.publishing_queue = parent.publishing_queue
            self.zmq_port = parent.zmq_port
            self.tree = parent.tree
            self.tree_lock = parent.tree_lock

    def publish(self, topic: str, val: Any) -> None:
        """Thread-safe publication of data to ZMQ and local tree"""
        topic_path = self.z_path + str(topic)
        encoded_msg = yaml.safe_dump(val).encode()
        
        # Update local tree
        with self.tree_lock:
            self.tree[topic_path] = val
            
        # Publish to ZMQ
        self.publishing_queue.put([topic_path.encode(), encoded_msg])

    def get_value(self, topic: str, default: Any = None) -> Any:
        """Thread-safe access to tree data"""
        with self.tree_lock:
            return self.tree.get(self.z_path + str(topic), default)

    def __del__(self):
        """Cleanup when node is destroyed"""
        if hasattr(self, 'kill_thread'):
            self.kill_thread = True
            if hasattr(self, 'z_pub_thread'):
                self.z_pub_thread.join()
            if hasattr(self, 'z_recv_thread'):
                self.z_recv_thread.join()
