"""
This file implements a self-contained widget for image annotation using PyQt5.

Features:
1. Display an image as background using QGraphicsView and QGraphicsScene
2. Allow drawing and editing of points (as crosses), lines, and rectangles on the image
3. Switch between different annotation modes
4. Temporary shapes in red, finalized shapes in blue
5. All shapes have 1 pixel thickness
6. Temporary point is a larger cross, finalized point is a smaller cross
7. All shapes (including points) have a temporary state while dragging
8. Integrated controls for mode selection and clearing annotations
9. Annotations remain correctly positioned when resizing the widget
10. Selection tool to select and move existing annotations
11. Delete tool to delete selected annotations
12. Selected annotations are highlighted in red, while unselected annotations remain blue
13. Keyboard shortcuts for select mode (S) and delete (Del)
14. Add labels to annotations with option to show/hide labels

Constraints and Considerations:
1. Uses PyQt5 for the GUI
2. Implements custom painting for annotations using QGraphicsItems
3. Supports multiple annotation types (point, line, rectangle)
4. Provides a clear and intuitive user interface
5. Allows for easy extension with additional features in the future
6. Uses QGraphicsView and QGraphicsScene for improved rendering and scaling
7. Ensures proper color management for selected and unselected annotations
"""

from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QButtonGroup,
                             QGraphicsView, QGraphicsScene, QGraphicsItem, QGraphicsPixmapItem,
                             QShortcut, QApplication, QMainWindow, QGraphicsRectItem, QGraphicsLineItem, QGraphicsEllipseItem, QGraphicsTextItem, QGraphicsItemGroup, QCheckBox, QLineEdit, QLabel)
from PyQt5.QtGui import QPixmap, QPen, QColor, QPainter, QKeySequence, QFont, QPainterPath
from PyQt5.QtCore import Qt, QPointF, QRectF, pyqtSignal, QSizeF, QLineF
from math import sqrt
import os
import random
import sys

class AnnotationItem(QGraphicsItem):
    def __init__(self, item_type, data, color=Qt.blue, label=None):
        super().__init__()
        self.item_type = item_type
        self.data = data
        self.original_color = QColor(color)
        self.selected_color = QColor(Qt.red)
        self.current_color = self.original_color
        self.label = label
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        
        self.shape_item = None
        self.label_item = None
        self.create_shape()
        if label:
            self.set_label(label)

    def create_shape(self):
        pen = QPen(self.current_color, 1)
        if self.item_type == "point":
            self.shape_item = QGraphicsItemGroup(self)
            self.horizontal_line = QGraphicsLineItem(QLineF(-5, 0, 5, 0), self.shape_item)
            self.vertical_line = QGraphicsLineItem(QLineF(0, -5, 0, 5), self.shape_item)
            self.horizontal_line.setPen(pen)
            self.vertical_line.setPen(pen)
            self.setPos(self.data)
        elif self.item_type == "line":
            self.shape_item = QGraphicsLineItem(QLineF(self.data[0], self.data[1]), self)
            self.shape_item.setPen(pen)
        elif self.item_type == "rectangle":
            rect = QRectF(self.data[0], self.data[1]).normalized()
            self.shape_item = QGraphicsRectItem(rect, self)
            self.shape_item.setPen(pen)

    def set_label(self, label):
        self.label = label
        self._create_label_item()

    def _create_label_item(self):
        if self.label_item:
            self.scene().removeItem(self.label_item)
        if self.label:
            self.label_item = QGraphicsTextItem(self.label, self)
            font = QFont()
            font.setPointSize(8)
            self.label_item.setFont(font)
            self.label_item.setDefaultTextColor(self.current_color)
            self.update_label_position()

    def update_label_position(self):
        if self.label_item and self.scene():
            scene_rect = self.scene().sceneRect()
            item_rect = self.sceneBoundingRect()
            label_width = self.label_item.boundingRect().width()
            label_height = self.label_item.boundingRect().height()

            # Calcul de la position du label en coordonnées de la scène
            if item_rect.right() + label_width > scene_rect.right():
                # Place le label au-dessus de l'annotation
                label_pos = QPointF(item_rect.left(), item_rect.top() - label_height)
            else:
                # Place le label en haut à droite de l'annotation
                label_pos = QPointF(item_rect.right(), item_rect.top())

            # Conversion des coordonnées de la scène en coordonnées locales de l'item
            self.label_item.setPos(self.mapFromScene(label_pos))

    def boundingRect(self):
        if self.item_type == "point":
            return QRectF(-5, -5, 10, 10)
        elif self.shape_item:
            return self.shape_item.boundingRect()
        return QRectF()

    def shape(self):
        path = QPainterPath()
        if self.item_type == "point":
            path.addRect(self.boundingRect())
        elif self.item_type == "line":
            path.moveTo(self.data[0])
            path.lineTo(self.data[1])
            path.addRect(self.boundingRect().adjusted(-2, -2, 2, 2))  # Élargir la zone de sélection
        elif self.item_type == "rectangle":
            path.addRect(self.boundingRect())
        return path

    def paint(self, painter, option, widget):
        # Cette méthode est vide car le dessin est géré par les éléments enfants
        pass

    def set_color(self, color):
        self.original_color = QColor(color)
        self.current_color = self.original_color if not self.isSelected() else self.selected_color
        if self.item_type == "point":
            pen = QPen(self.current_color, 1)
            self.horizontal_line.setPen(pen)
            self.vertical_line.setPen(pen)
        elif self.shape_item:
            self.shape_item.setPen(QPen(self.current_color, 1))
        if self.label_item:
            self.label_item.setDefaultTextColor(self.current_color)

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemSelectedHasChanged:
            self.update_color()
        elif change == QGraphicsItem.ItemSceneHasChanged and self.scene():
            self._create_label_item()
        return super().itemChange(change, value)

    def update_color(self):
        self.current_color = self.selected_color if self.isSelected() else self.original_color
        self.set_color(self.current_color)

    def update_position(self, pos):
        if self.item_type == "point":
            self.setPos(pos)
        elif self.item_type in ["line", "rectangle"]:
            if self.shape_item:
                if self.item_type == "line":
                    self.shape_item.setLine(QLineF(self.data[0], pos))
                elif self.item_type == "rectangle":
                    rect = QRectF(self.data[0], pos).normalized()
                    self.shape_item.setRect(rect)
                self.data[1] = pos

    def set_label_visible(self, visible):
        if self.label_item:
            self.label_item.setVisible(visible)

class CustomGraphicsView(QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent_widget = parent

    def mousePressEvent(self, event):
        self.parent_widget.mousePressEvent(event)

    def mouseMoveEvent(self, event):
        self.parent_widget.mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        self.parent_widget.mouseReleaseEvent(event)

class AnnotationWidget(QWidget):
    annotation_changed = pyqtSignal(list)

    def __init__(self, db_manager):
        super().__init__()
        self.db_manager = db_manager
        self.scene = QGraphicsScene()
        self.view = CustomGraphicsView(self)
        self.view.setScene(self.scene)
        self.view.setRenderHint(QPainter.Antialiasing)
        self.view.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.image_item = None
        self.current_item = None
        self.mode = "point"
        self.drawing = False
        self.selected_item = None
        self.show_labels = True
        self.current_label = ""

        self.init_ui()

    def init_ui(self):
        main_layout = QHBoxLayout()
        self.setLayout(main_layout)

        main_layout.addWidget(self.view, 1)

        # Controls
        button_layout = QVBoxLayout()
        main_layout.addLayout(button_layout)

        self.button_group = QButtonGroup()
        self.button_group.setExclusive(True)

        point_button = QPushButton("Point")
        line_button = QPushButton("Line")
        rectangle_button = QPushButton("Rectangle")
        select_button = QPushButton("Select")
        delete_button = QPushButton("Delete")
        clear_button = QPushButton("Clear")

        for button in [point_button, line_button, rectangle_button, select_button]:
            button.setCheckable(True)
            self.button_group.addButton(button)

        point_button.setChecked(True)  # Default mode
        
        point_button.clicked.connect(lambda: self.set_mode("point"))
        line_button.clicked.connect(lambda: self.set_mode("line"))
        rectangle_button.clicked.connect(lambda: self.set_mode("rectangle"))
        select_button.clicked.connect(lambda: self.set_mode("select"))
        delete_button.clicked.connect(self.delete_selected_item)
        clear_button.clicked.connect(self.clear_annotations)

        button_layout.addWidget(point_button)
        button_layout.addWidget(line_button)
        button_layout.addWidget(rectangle_button)
        button_layout.addWidget(select_button)
        button_layout.addWidget(delete_button)
        button_layout.addWidget(clear_button)
        button_layout.addStretch()

        self.setMinimumSize(600, 400)

        # Keyboard shortcuts
        select_button.setShortcut(QKeySequence("S"))
        delete_shortcut = QShortcut(QKeySequence(Qt.Key_Delete), self)
        delete_shortcut.activated.connect(self.delete_selected_item)

        # Nouvelle checkbox pour afficher/masquer les labels
        self.show_labels_checkbox = QCheckBox("Show Labels")
        self.show_labels_checkbox.setChecked(True)
        self.show_labels_checkbox.stateChanged.connect(self.toggle_labels)
        button_layout.addWidget(self.show_labels_checkbox)

        # Nouvelle textbox pour entrer le label
        label_layout = QHBoxLayout()
        label_layout.addWidget(QLabel("Label:"))
        self.label_input = QLineEdit()
        self.label_input.textChanged.connect(self.update_current_label)
        label_layout.addWidget(self.label_input)
        button_layout.addLayout(label_layout)

    def set_mode(self, mode):
        self.mode = mode
        if mode != "select" and self.selected_item:
            self.selected_item.setSelected(False)
            self.selected_item.update_color()
            self.selected_item = None

    def mousePressEvent(self, event):
        if self.view.viewport().rect().contains(event.pos()):
            scene_pos = self.view.mapToScene(event.pos())
            if event.button() == Qt.LeftButton:
                if self.mode == "select":
                    self.select_nearest_item(scene_pos)
                else:
                    self.drawing = True
                    if self.mode == "point":
                        self.current_item = AnnotationItem("point", scene_pos, Qt.blue, self.current_label)
                    elif self.mode in ["line", "rectangle"]:
                        self.current_item = AnnotationItem(self.mode, [scene_pos, scene_pos], Qt.blue, self.current_label)
                    self.scene.addItem(self.current_item)
                    self.current_item.set_label_visible(self.show_labels)

    def mouseMoveEvent(self, event):
        if self.drawing and self.view.viewport().rect().contains(event.pos()):
            scene_pos = self.view.mapToScene(event.pos())
            if self.current_item:
                self.current_item.update_position(scene_pos)

    def mouseReleaseEvent(self, event):
        if self.drawing:
            self.drawing = False
            if self.current_item:
                self.current_item.update_label_position()
                self.annotation_changed.emit(self.get_annotations())
            self.current_item = None

    def select_nearest_item(self, pos):
        items = self.scene.items(pos)
        nearest_item = None
        min_distance = float('inf')
        for item in items:
            if isinstance(item, AnnotationItem):
                distance = self.distance_to_item(pos, item)
                if distance < min_distance:
                    min_distance = distance
                    nearest_item = item
        
        # Désélectionner l'élément précédemment sélectionné
        if self.selected_item:
            self.selected_item.setSelected(False)
            self.selected_item.update_color()
        
        # Sélectionner le nouvel élément s'il y en a un
        if nearest_item:
            nearest_item.setSelected(True)
            nearest_item.update_color()
            self.selected_item = nearest_item
        else:
            self.selected_item = None
    
    def distance_to_item(self, pos, item):
        if item.item_type == "point":
            return (pos - item.scenePos()).manhattanLength()
        elif item.item_type == "line":
            line = QLineF(item.data[0], item.data[1])
            return self.distance_point_to_line(pos, line)
        elif item.item_type == "rectangle":
            rect = QRectF(item.data[0], item.data[1]).normalized()
            return self.distance_point_to_rect(pos, rect)

    def distance_point_to_line(self, point, line):
        p1 = line.p1()
        p2 = line.p2()
        d = line.length()
        t = ((point.x() - p1.x()) * (p2.x() - p1.x()) + (point.y() - p1.y()) * (p2.y() - p1.y())) / (d * d)
        t = max(0, min(1, t))
        projection = QPointF(p1.x() + t * (p2.x() - p1.x()), p1.y() + t * (p2.y() - p1.y()))
        return (point - projection).manhattanLength()

    def distance_point_to_rect(self, point, rect):
        if rect.contains(point):
            return 0
        dx = max(rect.left() - point.x(), 0, point.x() - rect.right())
        dy = max(rect.top() - point.y(), 0, point.y() - rect.bottom())
        return (dx**2 + dy**2)**0.5

    def clear_annotations(self):
        self.scene.clear()
        self.current_item = None

    def load_image(self, image_path):
        pixmap = QPixmap(image_path)
        if not pixmap.isNull():
            if self.image_item:
                self.scene.removeItem(self.image_item)
            self.image_item = QGraphicsPixmapItem(pixmap)
            self.scene.addItem(self.image_item)
            self.fit_image_in_view()

    def fit_image_in_view(self):
        if self.image_item:
            self.view.fitInView(self.image_item, Qt.KeepAspectRatio)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.fit_image_in_view()
        # Mettre à jour la position des labels après le redimensionnement
        for item in self.scene.items():
            if isinstance(item, AnnotationItem):
                item.update_label_position()

    def showEvent(self, event):
        super().showEvent(event)
        self.fit_image_in_view()

    def load_annotations(self, annotations):
        for ann in annotations:
            ann_type, x1, y1, x2, y2, class_name = ann
            color = self.db_manager.get_class_color(class_name)
            if not color:
                color = QColor(Qt.blue)
            if ann_type == "rectangle":
                item = AnnotationItem("rectangle", [QPointF(x1, y1), QPointF(x2, y2)], color, class_name)
            elif ann_type == "point":
                item = AnnotationItem("point", QPointF(x1, y1), color, class_name)
            elif ann_type == "line":
                item = AnnotationItem("line", [QPointF(x1, y1), QPointF(x2, y2)], color, class_name)
            self.scene.addItem(item)
            item.set_label_visible(self.show_labels)

    def get_annotations(self):
        annotations = []
        for item in self.scene.items():
            if isinstance(item, AnnotationItem):
                if item.item_type == "point":
                    annotations.append(('point', item.pos().x(), item.pos().y(), None, None))
                elif item.item_type in ["line", "rectangle"]:
                    annotations.append((item.item_type, item.data[0].x(), item.data[0].y(), item.data[1].x(), item.data[1].y()))
        return annotations

    def delete_selected_item(self):
        if self.selected_item:
            self.scene.removeItem(self.selected_item)
            self.selected_item = None
            self.annotation_changed.emit(self.get_annotations())

    def cleanup(self):
        if self.scene:
            self.scene.clear()
        self.current_item = None
        self.selected_item = None
        self.image_item = None

    def toggle_labels(self, state):
        self.show_labels = state == Qt.Checked
        for item in self.scene.items():
            if isinstance(item, AnnotationItem):
                item.set_label_visible(self.show_labels)

    def update_current_label(self, text):
        self.current_label = text

    def clear_scene(self):
        if self.image_item:
            self.scene.removeItem(self.image_item)
            self.image_item = None
        self.scene.clear()

    def update_annotation_color(self, class_name, color):
        for item in self.scene.items():
            if isinstance(item, AnnotationItem) and item.label == class_name:
                item.set_color(color)
        self.scene.update()

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Image Annotation Tool")
        self.setGeometry(100, 100, 800, 600)

        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        layout = QVBoxLayout(central_widget)

        self.annotation_widget = AnnotationWidget(self.db_manager)
        layout.addWidget(self.annotation_widget)

        change_image_button = QPushButton("Change Image")
        change_image_button.clicked.connect(self.change_image)
        layout.addWidget(change_image_button)

        add_annotation_button = QPushButton("Add Annotation")
        add_annotation_button.clicked.connect(self.add_annotation)
        layout.addWidget(add_annotation_button)

        self.image_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "new_dataset", "images"))
        self.images = [f for f in os.listdir(self.image_dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
        
        if self.images:
            self.change_image()
        else:
            print(f"No images found in {self.image_dir}")

    def change_image(self):
        self.annotation_widget.cleanup()
        if self.images:
            random_image = random.choice(self.images)
            image_path = os.path.join(self.image_dir, random_image)
            self.annotation_widget.load_image(image_path)
            print(f"Changed to image: {image_path}")
        else:
            print(f"No images available in {self.image_dir}")

    def add_annotation(self):
        annotations = self.annotation_widget.get_annotations()
        new_annotation = ('point', 100 + len(annotations) * 50, 100, None, None)
        annotations.append(new_annotation)
        self.annotation_widget.load_annotations(annotations)
        print(f"Added annotation: {new_annotation}")

if __name__ == "__main__":
    print("Starting application")
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    print("Entering event loop")
    sys.exit(app.exec_())
