"""
Dataset Explorer GUI

This module provides a graphical user interface for exploring, creating, and editing datasets used in image annotation projects.
It integrates with existing AnnotationGUI and Database classes for seamless interaction with the dataset.

Features:
1. Display all images in a single list, with annotated images in bold
2. Show annotations from the database
3. Allow manual annotation
4. Load and display inference results from a YOLOv5 model
5. Toggle between showing database annotations and inference results
6. Create new datasets
7. Import images to existing datasets
8. Load existing datasets
9. Manage annotation classes with associated colors and shapes
10. Automatically populate existing annotation classes when opening a dataset

Design decisions and constraints:
1. Use PyQt5 for the graphical interface
2. Integrate AnnotationWidget for image display and annotation
3. Use the Database class for all database interactions
4. Implement a single-window interface with a split view: image list on the left, annotation widget on the right
5. Include a resizable splitter between the left panel (image list) and the right panel (annotation widget)
6. Load YOLOv5 model for inference
7. Display inference results as green bounding boxes
8. Do not save inference results to the database
9. Maintain existing functionality for dataset creation and image import
10. Add a new tab for managing annotation classes with color and shape selection
11. Populate existing annotation classes from the database when loading a dataset
"""

import os
import random
from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout, QHBoxLayout, QListWidget, QLabel, QPushButton, 
                             QFileDialog, QInputDialog, QMessageBox, QSplitter, QMenuBar, QAction, QListWidgetItem, 
                             QCheckBox, QTabWidget, QTableWidget, QTableWidgetItem, QComboBox, QColorDialog, QRadioButton)
from PyQt5.QtCore import Qt, QSettings, pyqtSignal, QPointF
from PyQt5.QtGui import QFont, QColor
from annotation import AnnotationWidget, AnnotationItem
from database import DatabaseManager
from inference import InferenceModule
import faulthandler
faulthandler.enable()

class DatasetExplorerGUI(QWidget):
    dataset_changed = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.dataset_path = None
        self.db_manager = None
        self.settings = QSettings("YourCompany", "DatasetExplorer")
        self.inference_module = InferenceModule()
        self.inference_annotations = []
        self.annotation_classes = []
        
        self.init_ui()
        self.load_dataset(self.settings.value("last_dataset"))

    def init_ui(self):
        """Initialize the user interface"""
        self.setWindowTitle('Dataset Explorer')
        self.setGeometry(100, 100, 1200, 800)

        main_layout = QVBoxLayout()

        # Menu bar
        self.create_menu()

        # Main content area with splitter
        self.splitter = QSplitter(Qt.Horizontal)

        # Left panel: image list and controls
        left_widget = QWidget()
        left_layout = QVBoxLayout(left_widget)

        # Checkboxes
        self.sort_annotated_checkbox = QCheckBox("Show annotated images first")
        self.sort_annotated_checkbox.stateChanged.connect(self.load_images)
        left_layout.addWidget(self.sort_annotated_checkbox)

        self.inference_checkbox = QCheckBox("Show inference results")
        self.inference_checkbox.stateChanged.connect(self.toggle_inference)
        left_layout.addWidget(self.inference_checkbox)

        # Modifier la liste d'images pour permettre la sélection multiple
        self.image_list = QListWidget()
        self.image_list.setSelectionMode(QListWidget.ExtendedSelection)
        self.image_list.currentItemChanged.connect(self.on_image_selected)
        left_layout.addWidget(self.image_list)

        # Ajouter le menu déroulant pour le tri
        sort_layout = QHBoxLayout()
        sort_label = QLabel("Sort by:")
        self.sort_combo = QComboBox()
        self.sort_combo.addItems(["Name", "Dataset", "Annotation"])
        self.sort_combo.currentTextChanged.connect(self.sort_images)
        sort_layout.addWidget(sort_label)
        sort_layout.addWidget(self.sort_combo)
        left_layout.addLayout(sort_layout)

        # Remplacer les radio buttons par des boutons normaux
        set_type_widget = QWidget()
        set_type_layout = QHBoxLayout()
        set_type_widget.setLayout(set_type_layout)

        self.train_button = QPushButton("Train")
        self.val_button = QPushButton("Val")
        self.train_button.clicked.connect(lambda: self.set_selected_images_type('train'))
        self.val_button.clicked.connect(lambda: self.set_selected_images_type('val'))
        
        # Styliser les boutons
        self.train_button.setStyleSheet("QPushButton { background-color: #ff0000; color: white; }")
        self.val_button.setStyleSheet("QPushButton { background-color: #0000ff; color: white; }")
        
        set_type_layout.addWidget(self.train_button)
        set_type_layout.addWidget(self.val_button)
        
        # Ajouter le compteur
        self.set_stats_label = QLabel("Train: 0 | Val: 0")
        set_type_layout.addWidget(self.set_stats_label)
        
        left_layout.addWidget(set_type_widget)

        self.splitter.addWidget(left_widget)

        # Right panel: tabs for annotation and class management
        right_widget = QTabWidget()
        
        # Annotation widget
        self.annotation_widget = AnnotationWidget(self.db_manager)
        self.annotation_widget.annotation_changed.connect(self.save_annotations)
        right_widget.addTab(self.annotation_widget, "Annotation")

        # Class management widget
        self.class_management_widget = self.create_class_management_widget()
        right_widget.addTab(self.class_management_widget, "Class Management")

        self.splitter.addWidget(right_widget)

        # Set initial sizes for splitter (adjust as needed)
        self.splitter.setSizes([400, 800])

        main_layout.addWidget(self.splitter)

        self.setLayout(main_layout)

        # Connect the dataset_changed signal to update the UI
        self.dataset_changed.connect(self.update_ui)

    def create_class_management_widget(self):
        widget = QWidget()
        layout = QVBoxLayout(widget)

        self.class_table = QTableWidget()
        self.class_table.setColumnCount(4)
        self.class_table.setHorizontalHeaderLabels(["Class Name", "Color", "Shape", "Actions"])
        layout.addWidget(self.class_table)

        add_class_button = QPushButton("Add Class")
        add_class_button.clicked.connect(self.add_class)
        layout.addWidget(add_class_button)

        return widget

    def add_class(self, class_name=None, color=None, populate=False):
        if not class_name and not populate:
            class_name, ok = QInputDialog.getText(self, "Add Class", "Enter class name:")
            if not ok or not class_name:
                return

        # Vérifier si la classe existe déjà
        for i in range(self.class_table.rowCount()):
            if self.class_table.item(i, 0).text() == class_name:
                print(f"Class {class_name} already exists")  # Message de débogage
                return

        row_position = self.class_table.rowCount()
        self.class_table.insertRow(row_position)
        
        self.class_table.setItem(row_position, 0, QTableWidgetItem(class_name))
        
        color_button = QPushButton()
        color_button.clicked.connect(lambda: self.choose_color(row_position))
        self.class_table.setCellWidget(row_position, 1, color_button)
        
        shape_combo = QComboBox()
        shape_combo.addItems(["Point", "Line", "Rectangle"])
        self.class_table.setCellWidget(row_position, 2, shape_combo)

        if not color:
            color = self.generate_random_color()

        shape = "Rectangle"  # Forme par défaut

        self.annotation_classes.append({
            "name": class_name,
            "color": color,
            "shape": shape
        })
        self.update_color_button(row_position)

        if not populate:
            self.db_manager.add_class(class_name, color.name(), shape)

        print(f"Added class: {class_name}")  # Message de débogage

    def generate_random_color(self):
        return QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    def choose_color(self, row):
        color = QColorDialog.getColor()
        if color.isValid():
            class_name = self.class_table.item(row, 0).text()
            self.annotation_classes[row]["color"] = color
            self.update_color_button(row)
            self.update_annotation_colors(class_name, color)
            
            # Mettre à jour la couleur dans la base de données
            self.db_manager.update_class_color(class_name, color)

    def update_annotation_colors(self, class_name, color):
        if self.annotation_widget:
            self.annotation_widget.update_annotation_color(class_name, color)

    def update_color_button(self, row):
        color_button = self.class_table.cellWidget(row, 1)
        color = self.annotation_classes[row]["color"]
        color_button.setStyleSheet(f"background-color: {color.name()};")

    def delete_class(self, row):
        self.class_table.removeRow(row)
        del self.annotation_classes[row]

    def load_images(self):
        self.image_list.clear()
        if self.dataset_path:
            images_dir = os.path.join(self.dataset_path, 'images')
            if os.path.exists(images_dir):
                for filename in os.listdir(images_dir):
                    if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                        item = QListWidgetItem(filename)
                        image_path = os.path.join('images', filename)
                        item.setData(Qt.UserRole, image_path)
                        self.image_list.addItem(item)
                        self.update_image_item_style(image_path)
                self.sort_images()
                self.update_set_stats()

    def toggle_inference(self, state):
        if state == Qt.Checked and not self.inference_module.model:
            # Changement ici : utilisation d'un chemin absolu relatif au répertoire de l'application
            model_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "models", "exp167.pt"))
            if os.path.exists(model_path):
                self.inference_module.load_model(model_path)
            else:
                QMessageBox.warning(self, "Model Not Found", f"The model file {model_path} was not found.")
                self.inference_checkbox.setChecked(False)
                return

        self.on_image_selected(self.image_list.currentItem(), None)

    def on_image_selected(self, current, previous):
        if current is None:
            return

        self.current_image = current.data(Qt.UserRole)
        full_path = os.path.join(self.dataset_path, self.current_image)
        
        # Nettoyer la scène avant de charger la nouvelle image et ses annotations
        self.annotation_widget.clear_scene()
        
        # Charger la nouvelle image
        self.annotation_widget.load_image(full_path)
        
        # Charger les annotations
        annotations = self.db_manager.get_image_annotations(self.current_image)
        self.annotation_widget.load_annotations(annotations)

        # Mettre à jour l'état des boutons train/val
        is_test = self.db_manager.is_image_test(self.current_image)
        self.update_image_item_style(self.current_image)

        # Exécuter l'inférence si nécessaire
        if self.inference_checkbox.isChecked() and self.inference_module.model:
            self.inference_annotations = self.inference_module.run_inference(full_path)
            self.display_inference_results()

    def display_inference_results(self):
        for ann in self.inference_annotations:
            x1, y1, x2, y2, conf, cls = ann
            class_name = self.inference_module.model.names[int(cls)]  # Obtenir le nom de la classe
            item = AnnotationItem("rectangle", [QPointF(x1, y1), QPointF(x2, y2)], QColor(0, 255, 0), f"{class_name} {conf:.2f}")
            self.annotation_widget.scene.addItem(item)

    def save_annotations(self, annotations):
        if self.current_image:
            try:
                float_annotations = []
                for ann_type, x1, y1, x2, y2, class_name in annotations:
                    color = next((cls["color"] for cls in self.annotation_classes if cls["name"] == class_name), QColor(Qt.blue))
                    float_annotations.append((ann_type, float(x1), float(y1), float(x2) if x2 is not None else None, float(y2) if y2 is not None else None, class_name, color.name()))
                self.db_manager.save_annotations(self.current_image, float_annotations)
                self.update_image_item_style(self.current_image)
            except Exception as e:
                print(f"Error saving annotations: {e}")

    def update_image_item_style(self, image_path):
        for i in range(self.image_list.count()):
            item = self.image_list.item(i)
            if item.data(Qt.UserRole) == image_path:
                # Vérifier si l'image a des annotations
                has_annotations = len(self.db_manager.get_image_annotations(image_path)) > 0
                
                # Configurer la police
                font = item.font()
                font.setBold(has_annotations)
                item.setFont(font)
                
                # Configurer la couleur selon is_train/is_test
                is_test = self.db_manager.is_image_test(image_path)
                if is_test:
                    item.setForeground(QColor(0, 0, 255))  # Bleu pour validation
                else:
                    item.setForeground(QColor(255, 0, 0))  # Rouge pour training
                break

    def load_dataset(self, path=None):
        if not path:
            path = QFileDialog.getExistingDirectory(self, "Select Dataset Folder")
        if path:
            self.dataset_path = path
            self.db_manager = DatabaseManager(os.path.join(self.dataset_path, 'dataset.db'))
            self.annotation_widget.db_manager = self.db_manager
            self.dataset_changed.emit(self.dataset_path)
            self.settings.setValue("last_dataset", self.dataset_path)
            self.populate_existing_classes()
            self.load_images()

    def populate_existing_classes(self):
        if self.db_manager:
            existing_classes = self.db_manager.get_existing_classes()
            self.annotation_classes.clear()
            self.class_table.setRowCount(0)
            for class_name in existing_classes:
                color = self.db_manager.get_class_color(class_name)
                if not color:
                    color = self.generate_random_color()
                    self.db_manager.add_class(class_name, color.name(), "Rectangle")
                self.add_class(class_name, color, populate=True)

    def update_ui(self):
        self.load_images()

    def create_new_dataset(self):
        name, ok = QInputDialog.getText(self, 'Create New Dataset', 'Enter dataset name:')
        if ok and name:
            root_dir = QFileDialog.getExistingDirectory(self, "Select Root Directory for New Dataset")
            if root_dir:
                dataset_path = os.path.join(root_dir, name)
                try:
                    os.makedirs(dataset_path)
                    os.makedirs(os.path.join(dataset_path, 'images'))
                    
                    db_path = os.path.join(dataset_path, 'dataset.db')
                    DatabaseManager.create_database(db_path)
                    
                    QMessageBox.information(self, "Success", f"Dataset '{name}' created successfully!")
                    self.load_dataset(dataset_path)
                except Exception as e:
                    QMessageBox.critical(self, "Error", f"Failed to create dataset: {str(e)}")

    def import_images(self):
        if not self.dataset_path:
            QMessageBox.warning(self, "Warning", "Please load a dataset first.")
            return

        import_dir = QFileDialog.getExistingDirectory(self, "Select Directory to Import Images From")
        if import_dir:
            images_dir = os.path.join(self.dataset_path, 'images')
            imported_count = self.db_manager.import_images(import_dir, images_dir)
            QMessageBox.information(self, "Import Complete", f"Imported {imported_count} images to the dataset.")
            self.load_images()

    def closeEvent(self, event):
        if self.db_manager:
            self.db_manager.close()
        event.accept()

    def create_menu(self):
        menubar = QMenuBar(self)
        
        # File menu
        file_menu = menubar.addMenu('File')
        
        load_action = QAction('Load Dataset', self)
        load_action.triggered.connect(self.load_dataset)
        file_menu.addAction(load_action)
        
        create_action = QAction('Create New Dataset', self)
        create_action.triggered.connect(self.create_new_dataset)
        file_menu.addAction(create_action)
        
        import_action = QAction('Import Images', self)
        import_action.triggered.connect(self.import_images)
        file_menu.addAction(import_action)
        
        exit_action = QAction('Exit', self)
        exit_action.triggered.connect(self.close)
        file_menu.addAction(exit_action)

    def on_set_type_changed(self):
        if not self.current_image:
            return
            
        new_set_type = 'val' if self.val_radio.isChecked() else 'train'
        self.db_manager.set_image_set_type(self.current_image, new_set_type)
        self.update_set_stats()
        self.update_image_item_style(self.current_image)

    def update_set_stats(self):
        train_count, test_count = self.db_manager.count_images_by_type()
        self.set_stats_label.setText(f"Train: {train_count} | Val: {test_count}")

    def set_selected_images_type(self, set_type):
        selected_items = self.image_list.selectedItems()
        for item in selected_items:
            image_path = item.data(Qt.UserRole)
            is_train = (set_type == 'train')
            is_test = not is_train
            self.db_manager.set_image_training_status(image_path, is_train, is_test)
            self.update_image_item_style(image_path)
        self.update_set_stats()

    def sort_images(self):
        current_sort = self.sort_combo.currentText()
        if current_sort == "Name":
            self.sort_images_by_name()
        elif current_sort == "Dataset":
            self.sort_images_by_dataset()
        else:  # "Annotation"
            self.sort_images_by_annotation()

    def sort_images_by_name(self):
        items = []
        for i in range(self.image_list.count()):
            item = self.image_list.takeItem(0)
            items.append(item)
        items.sort(key=lambda x: x.text())
        for item in items:
            self.image_list.addItem(item)

    def sort_images_by_dataset(self):
        items = []
        for i in range(self.image_list.count()):
            item = self.image_list.takeItem(0)
            image_path = item.data(Qt.UserRole)
            is_test = self.db_manager.is_image_test(image_path)
            items.append((item, is_test))
        
        # Trier d'abord par is_test (validation avant training) puis par nom
        items.sort(key=lambda x: (not x[1], x[0].text()))
        
        for item, _ in items:
            self.image_list.addItem(item)

    def sort_images_by_annotation(self):
        items = []
        for i in range(self.image_list.count()):
            item = self.image_list.takeItem(0)
            image_path = item.data(Qt.UserRole)
            annotation_count = len(self.db_manager.get_image_annotations(image_path))
            items.append((item, annotation_count))
        items.sort(key=lambda x: (-x[1], x[0].text()))  # Tri décroissant par nombre d'annotations
        for item, _ in items:
            self.image_list.addItem(item)

def main():
    import sys
    app = QApplication(sys.argv)
    explorer = DatasetExplorerGUI()
    explorer.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
