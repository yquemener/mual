"""
This file manages the database module of the image annotation tool designed to create training datasets for vision models.

Constraints and Considerations:
1. **Data Structure**:
    - Images: Now include resolution (width and height).
    - Annotations: As previously defined.
    - Classes: Store class names, colors, and shapes.
2. **Database**:
    - Uses SQLAlchemy ORM for database interactions.
    - Supports multiple SQLite databases (one per dataset).
3. **Functionality**:
    - Ability to add images with resolution and annotations.
    - Retrieve all annotations for a given image.
    - Add, retrieve, and manage annotation classes.
4. **Flexibility**:
    - DatabaseManager class allows specifying the database file at runtime.
5. **Maintenance and Extensibility**:
    - Well-structured for easy modifications and additions.
6. **Performance**:
    - Efficient session management to prevent memory leaks.

This updated version includes image resolution, annotation classes, and maintains the flexibility to manage multiple datasets.
"""

from sqlalchemy import create_engine, Column, Integer, String, Boolean, Float, Enum, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from datetime import datetime
import os
import shutil
from PIL import Image as PILImage
from sqlalchemy.exc import IntegrityError
from PyQt5.QtGui import QColor

Base = declarative_base()

class Image(Base):
    __tablename__ = 'images'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    path = Column(String, nullable=False, unique=True)
    is_train = Column(Boolean, default=True, nullable=False)
    is_test = Column(Boolean, default=False, nullable=False)
    width = Column(Integer, nullable=False)
    height = Column(Integer, nullable=False)
    
    annotations = relationship('Annotation', back_populates='image', cascade='all, delete-orphan')
    
    def __repr__(self):
        return f"<Image(id={self.id}, path='{self.path}', train={self.is_train}, test={self.is_test}, width={self.width}, height={self.height})>"

class Annotation(Base):
    __tablename__ = 'annotations'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    image_id = Column(Integer, ForeignKey('images.id'), nullable=False)
    type = Column(Enum('rectangle', 'point', 'line', name='annotation_types'), nullable=False)
    x1 = Column(Float, nullable=False)
    y1 = Column(Float, nullable=False)
    x2 = Column(Float, nullable=True)
    y2 = Column(Float, nullable=True)
    class_name = Column(String, nullable=False)
    date_added = Column(DateTime, default=datetime.utcnow, nullable=False)
    
    image = relationship('Image', back_populates='annotations')
    
    def __repr__(self):
        return (
            f"<Annotation(id={self.id}, type='{self.type}', x1={self.x1}, y1={self.y1}, "
            f"x2={self.x2}, y2={self.y2}, class_name='{self.class_name}', "
            f"date_added='{self.date_added}')>"
        )

class AnnotationClass(Base):
    __tablename__ = 'annotation_classes'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False, unique=True)
    color = Column(String, nullable=False)
    shape = Column(String, nullable=False)

class DatabaseManager:
    def __init__(self, db_file):
        """
        Initialize the DatabaseManager with a specific database file.
        
        :param db_file: Path to the SQLite database file
        """
        self.db_file = db_file
        self.engine = create_engine(f'sqlite:///{db_file}', echo=False)
        self.SessionLocal = sessionmaker(bind=self.engine)
        self.init_db()

    def init_db(self):
        """
        Initialize the database by creating all defined tables.
        """
        Base.metadata.create_all(self.engine)

    def add_image(self, path, width, height, is_train=True, is_test=False):
        """Add a new image to the database"""
        with self.SessionLocal() as session:
            image = Image(
                path=path,
                width=width,
                height=height,
                is_train=is_train,
                is_test=is_test
            )
            session.add(image)
            try:
                session.commit()
                return image
            except IntegrityError:
                session.rollback()
                return session.query(Image).filter_by(path=path).first()

    def add_annotation(self, image_id, annotation_type, x1, y1, x2, y2, class_name):
        """
        Add a new annotation to the database.
        
        :param image_id: ID of the associated image
        :param annotation_type: Type of annotation (rectangle, point, or line)
        :param x1, y1, x2, y2: Coordinates of the annotation
        :param class_name: Class name of the annotated object
        :return: The created Annotation object
        """
        with self.SessionLocal() as session:
            new_annotation = Annotation(
                image_id=image_id,
                type=annotation_type,
                x1=x1, y1=y1, x2=x2, y2=y2,
                class_name=class_name
            )
            session.add(new_annotation)
            session.commit()
            session.refresh(new_annotation)
            return new_annotation

    def get_image_annotations(self, image_path):
        with self.SessionLocal() as session:
            image = session.query(Image).filter_by(path=image_path).first()
            if image:
                annotations = session.query(Annotation).filter_by(image_id=image.id).all()
                ret = [
                    (ann.type, ann.x1, ann.y1, ann.x2, ann.y2, ann.class_name)
                    for ann in annotations
                ]
                return ret
            return []

    def get_candidate_images(self):
        with self.SessionLocal() as session:
            candidates = session.query(Image).filter_by(is_train=False, is_test=False).all()
            return candidates

    def get_keyframe_images(self):
        with self.SessionLocal() as session:
            return session.query(Image).filter(Image.is_train | Image.is_test).all()

    def get_image_by_path(self, path):
        with self.SessionLocal() as session:
            # Normalize the path to handle both absolute and relative paths
            normalized_path = os.path.normpath(path)
            return session.query(Image).filter(Image.path.like(f"%{normalized_path}")).first()

    def import_images(self, source_dir, dest_dir):
        """Import images from source directory to dataset"""
        imported_count = 0
        for filename in os.listdir(source_dir):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                source_path = os.path.join(source_dir, filename)
                dest_path = os.path.join(dest_dir, filename)
                
                if not os.path.exists(dest_path):
                    shutil.copy2(source_path, dest_path)
                    
                    # Get image dimensions
                    with PILImage.open(dest_path) as img:
                        width, height = img.size
                    
                    # Add to database
                    rel_path = os.path.join('images', filename)
                    self.add_image(rel_path, width, height)
                    imported_count += 1
        
        return imported_count

    def save_annotations(self, image_path, annotations):
        with self.SessionLocal() as session:
            image = session.query(Image).filter_by(path=image_path).first()
            if image:
                try:
                    # Remove existing annotations
                    session.query(Annotation).filter_by(image_id=image.id).delete()
                    
                    # Add new annotations
                    for ann_type, x1, y1, x2, y2, class_name in annotations:
                        new_annotation = Annotation(
                            image_id=image.id,
                            type=ann_type,
                            x1=x1, y1=y1, x2=x2, y2=y2,
                            class_name=class_name
                        )
                        session.add(new_annotation)
                    
                    session.commit()
                except Exception as e:
                    session.rollback()

    @staticmethod
    def create_database(db_path):
        engine = create_engine(f'sqlite:///{db_path}', echo=False)
        Base.metadata.create_all(engine)

    def close(self):
        self.engine.dispose()

    def get_all_images(self):
        with self.SessionLocal() as session:
            return session.query(Image).all()

    def image_has_annotations(self, image_id):
        with self.SessionLocal() as session:
            return session.query(Annotation).filter_by(image_id=image_id).count() > 0

    def update_image_path(self, old_path, new_path):
        with self.SessionLocal() as session:
            image = session.query(Image).filter_by(path=old_path).first()
            if image:
                image.path = new_path
                try:
                    session.commit()
                except IntegrityError:
                    session.rollback()

    def merge_annotations(self, existing_path, old_path, new_path):
        with self.SessionLocal() as session:
            existing_image = session.query(Image).filter_by(path=existing_path).first()
            old_image = session.query(Image).filter_by(path=old_path).first()

            if existing_image and old_image:
                # Transférer les annotations de l'ancienne image vers l'image existante
                for annotation in old_image.annotations:
                    annotation.image_id = existing_image.id

                # Supprimer l'ancienne image de la base de données
                session.delete(old_image)

                # Mettre à jour le chemin de l'image existante si nécessaire
                if existing_path != new_path:
                    existing_image.path = new_path

                try:
                    session.commit()
                except IntegrityError:
                    session.rollback()

    def get_existing_classes(self):
        with self.SessionLocal() as session:
            # Récupérer les noms de classes uniques à partir de la table annotation
            classes = session.query(Annotation.class_name).distinct().all()
            class_names = [cls[0] for cls in classes]  # Extraire les noms de classes
            return class_names

    def add_class(self, name, color, shape):
        with self.SessionLocal() as session:
            new_class = AnnotationClass(name=name, color=color, shape=shape)
            session.add(new_class)
            try:
                session.commit()
            except IntegrityError:
                session.rollback()

    def get_class_color(self, class_name):
        with self.SessionLocal() as session:
            class_info = session.query(AnnotationClass).filter_by(name=class_name).first()
            if class_info:
                return QColor(class_info.color)
            return None

    def update_class_color(self, class_name, color):
        with self.SessionLocal() as session:
            class_info = session.query(AnnotationClass).filter_by(name=class_name).first()
            if class_info:
                class_info.color = color.name()  # Stocke la couleur sous forme de chaîne
                session.commit()

    def count_images_by_type(self):
        with self.SessionLocal() as session:
            train_count = session.query(Image).filter_by(is_train=True).count()
            test_count = session.query(Image).filter_by(is_test=True).count()
            return train_count, test_count

    def set_image_training_status(self, image_path, is_train, is_test):
        with self.SessionLocal() as session:
            image = session.query(Image).filter_by(path=image_path).first()
            if image:
                image.is_train = is_train
                image.is_test = is_test
                session.commit()

    def is_image_test(self, image_path):
        with self.SessionLocal() as session:
            image = session.query(Image).filter_by(path=image_path).first()
            return image.is_test if image else False

# Usage example:
if __name__ == "__main__":
    db_manager = DatabaseManager("dataset1.db")
    
    # Add an image with resolution
    image = db_manager.add_image("/path/to/image.jpg", width=1920, height=1080, is_train=True)
    
    # Add an annotation
    annotation = db_manager.add_annotation(
        image_id=image.id,
        annotation_type="rectangle",
        x1=10, y1=20, x2=100, y2=200,
        class_name="car"
    )
    
    # Get annotations for the image
    annotations = db_manager.get_image_annotations(image.id)
    for ann in annotations:
        print(ann)

