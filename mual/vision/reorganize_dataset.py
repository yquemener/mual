"""
Reorganize Dataset Script

This script reorganizes a dataset by moving all images from 'Candidates' and 'Keyframes'
directories into a single 'images' directory. It also updates the database to reflect
these changes and merges annotations for duplicate images.

Features:
1. Moves images from 'Candidates' and 'Keyframes' to 'images' directory
2. Handles duplicate filenames by adding a counter
3. Updates image paths in the database
4. Merges annotations for images present in both 'Candidates' and 'Keyframes'
5. Removes empty 'Candidates' and 'Keyframes' directories

Constraints and Considerations:
1. Requires DatabaseManager class with appropriate methods
2. Assumes SQLite database named 'dataset.db' in the dataset directory
3. Handles potential database integrity errors
4. Prints status messages for each operation
"""

import os
import shutil
from database import DatabaseManager

def reorganize_dataset(dataset_path):
    # Chemins des répertoires
    candidates_dir = os.path.join(dataset_path, 'candidates')
    keyframes_dir = os.path.join(dataset_path, 'keyframes')
    images_dir = os.path.join(dataset_path, 'images')
    
    # Créer le nouveau répertoire 'images' s'il n'existe pas
    os.makedirs(images_dir, exist_ok=True)
    
    # Initialiser le gestionnaire de base de données
    db_manager = DatabaseManager(os.path.join(dataset_path, 'dataset.db'))
    
    # Dictionnaire pour stocker les chemins originaux des images
    original_paths = {}
    
    # Fonction pour déplacer les images et mettre à jour la base de données
    def move_images(source_dir):
        for filename in os.listdir(source_dir):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                src_path = os.path.join(source_dir, filename)
                dst_path = os.path.join(images_dir, filename)
                
                # Gérer les doublons
                counter = 1
                while os.path.exists(dst_path):
                    name, ext = os.path.splitext(filename)
                    dst_path = os.path.join(images_dir, f"{name}_{counter}{ext}")
                    counter += 1
                
                # Déplacer le fichier
                shutil.move(src_path, dst_path)
                
                # Mettre à jour le chemin dans la base de données
                old_relative_path = os.path.relpath(src_path, dataset_path)
                new_relative_path = os.path.relpath(dst_path, dataset_path)
                
                if filename in original_paths:
                    # Fusionner les annotations si l'image existe déjà
                    db_manager.merge_annotations(original_paths[filename], old_relative_path, new_relative_path)
                else:
                    try:
                        db_manager.update_image_path(old_relative_path, new_relative_path)
                    except Exception as e:
                        print(f"Error updating path for {old_relative_path}: {e}")
                
                original_paths[filename] = new_relative_path
    
    # Déplacer les images de Candidates et Keyframes
    move_images(candidates_dir)
    move_images(keyframes_dir)
    
    # Supprimer les anciens répertoires s'ils sont vides
    try:
        os.rmdir(candidates_dir)
        print(f"Removed empty directory: {candidates_dir}")
    except OSError:
        print(f"Could not remove directory: {candidates_dir}")
    
    try:
        os.rmdir(keyframes_dir)
        print(f"Removed empty directory: {keyframes_dir}")
    except OSError:
        print(f"Could not remove directory: {keyframes_dir}")
    
    print("Dataset reorganization complete.")

if __name__ == "__main__":
    dataset_path = input("Enter the path to your dataset: ")
    reorganize_dataset(dataset_path)
