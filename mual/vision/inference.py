"""
Inference Module for Object Detection using YOLOv5

This module provides a simple interface for loading a YOLOv5 model,
performing inference on images, and displaying the results.

Features:
- Load YOLOv5 models (custom or default)
- Load images (custom or default)
- Perform inference on images
- Display results with bounding boxes
- Simple GUI for interaction

Dependencies:
- PyQt5
- PIL
- torch
- YOLOv5 (as a submodule)

Usage:
Run this script directly to launch the GUI application.

Design decisions and constraints:
1. Uses PyQt5 for the graphical interface
2. Assumes YOLOv5 is available as a submodule in the 'yolov5' directory
3. Default model path is set to 'vision/models/exp167.pt'
4. Default images are loaded from 'vision/new_dataset/images' directory
5. Inference results are displayed as red bounding boxes on the image
6. The GUI allows for custom model and image loading as well as default options
7. Inference results are printed to console and displayed on the image
8. The image is scaled to fit the GUI while maintaining aspect ratio
9. Detected class labels are included with each annotation

Note: Ensure that the YOLOv5 submodule is properly initialized before running.
"""

import sys
import os
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QFileDialog
from PyQt5.QtGui import QPixmap, QPainter, QColor, QPen
from PyQt5.QtCore import Qt
from PIL import Image
import torch
import numpy as np
import cv2

# Hack to make PyQt and cv2 load simultaneously
from pathlib import Path
import PyQt5

os.environ["QT_QPA_PLATFORM_PLUGIN_PATH"] = os.fspath(
    Path(PyQt5.__file__).resolve().parent / "Qt5" / "plugins"
)

try:
    import torch
    from PIL import Image
    VISION_AVAILABLE = True
except ImportError:
    VISION_AVAILABLE = False

# Ajoutez le chemin du sous-module YOLOv5 au sys.path si disponible
current_dir = os.path.dirname(os.path.abspath(__file__))
yolov5_dir = os.path.join(current_dir, 'yolov5')
if os.path.exists(yolov5_dir):
    sys.path.append(yolov5_dir)

class InferenceModule:
    def __init__(self):
        self.model = None
        self.available = VISION_AVAILABLE

    def load_model(self, weights_path):
        if not self.available:
            print("Vision module is not available")
            return False
        try:
            self.model = torch.hub.load(yolov5_dir, 'custom', path=weights_path, source='local')
            print("Model loaded successfully")
            print(f"Model classes: {self.model.names}")
            return True
        except Exception as e:
            print(f"Error loading model: {e}")
            return False

    def run_inference(self, image):
        if not self.available or self.model is None:
            return []
        
        if isinstance(image, str):
            # Si l'entrée est un chemin de fichier
            image = Image.open(image)
        elif isinstance(image, np.ndarray):
            # Si l'entrée est une image OpenCV (numpy array)
            image = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        
        results = self.model(image)
        return results.xyxy[0].cpu().numpy()

class InferenceGUI(QWidget):
    def __init__(self):
        super().__init__()
        self.inference_module = InferenceModule()
        self.image_path = None
        self.annotations = []
        self.original_image_size = None
        self.init_ui()

    def init_ui(self):
        layout = QVBoxLayout()

        self.image_label = QLabel()
        layout.addWidget(self.image_label)

        # Model loading buttons
        model_layout = QHBoxLayout()
        load_model_btn = QPushButton('Load Model')
        load_model_btn.clicked.connect(self.load_model)
        model_layout.addWidget(load_model_btn)

        load_default_model_btn = QPushButton('Load Default Model')
        load_default_model_btn.clicked.connect(self.load_default_model)
        model_layout.addWidget(load_default_model_btn)
        layout.addLayout(model_layout)

        # Image loading buttons
        image_layout = QHBoxLayout()
        load_image_btn = QPushButton('Load Image')
        load_image_btn.clicked.connect(self.load_image)
        image_layout.addWidget(load_image_btn)

        load_default_image_btn = QPushButton('Load Default Image')
        load_default_image_btn.clicked.connect(self.load_default_image)
        image_layout.addWidget(load_default_image_btn)
        layout.addLayout(image_layout)

        run_inference_btn = QPushButton('Run Inference')
        run_inference_btn.clicked.connect(self.run_inference)
        layout.addWidget(run_inference_btn)

        self.setLayout(layout)
        self.setWindowTitle('Inference Module GUI')
        self.setGeometry(300, 300, 400, 400)

    def load_model(self):
        weights_path, _ = QFileDialog.getOpenFileName(self, 'Load Model Weights', '', 'PT files (*.pt)')
        if weights_path:
            self.inference_module.load_model(weights_path)

    def load_default_model(self):
        default_model_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "models", "exp167.pt"))
        if os.path.exists(default_model_path):
            self.inference_module.load_model(default_model_path)
            print(f"Default model loaded: {default_model_path}")
        else:
            print(f"Default model not found at: {default_model_path}")

    def load_image(self):
        self.image_path, _ = QFileDialog.getOpenFileName(self, 'Load Image', '', 'Image files (*.jpg *.png)')
        if self.image_path:
            self.load_and_display_image()

    def load_default_image(self):
        default_image_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "vision", "new_dataset", "images"))
        if os.path.exists(default_image_dir):
            image_files = [f for f in os.listdir(default_image_dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]
            if image_files:
                self.image_path = os.path.join(default_image_dir, image_files[0])
                self.load_and_display_image()
                print(f"Default image loaded: {self.image_path}")
            else:
                print(f"No image files found in: {default_image_dir}")
        else:
            print(f"Default image directory not found: {default_image_dir}")

    def load_and_display_image(self):
        self.annotations = []  # Réinitialiser les annotations
        pixmap = QPixmap(self.image_path)
        self.original_image_size = pixmap.size()  # Stocker la taille originale
        scaled_pixmap = pixmap.scaled(400, 300, Qt.KeepAspectRatio)
        self.image_label.setPixmap(scaled_pixmap)
        print(f"Image loaded: {self.image_path}")
        print(f"Original size: {self.original_image_size.width()}x{self.original_image_size.height()}")

    def run_inference(self):
        if self.image_path and self.inference_module.model:
            self.annotations = self.inference_module.run_inference(self.image_path)
            print("Annotations:")
            for i, ann in enumerate(self.annotations):
                x1, y1, x2, y2, conf, cls = ann
                class_name = self.inference_module.model.names[int(cls)]  # Obtenir le nom de la classe
                print(f"  {i+1}: Class {class_name}, Confidence {conf:.2f}, Bbox: ({x1:.1f}, {y1:.1f}, {x2:.1f}, {y2:.1f})")
            self.update()  # Ceci déclenchera un nouveau paintEvent

    def paintEvent(self, event):
        super().paintEvent(event)
        if self.image_label.pixmap() is not None and self.annotations is not None:
            painter = QPainter(self.image_label.pixmap())
            painter.setPen(QPen(QColor(255, 0, 0), 2, Qt.SolidLine))

            # Calculer le facteur d'échelle
            current_size = self.image_label.pixmap().size()
            scale_x = current_size.width() / self.original_image_size.width()
            scale_y = current_size.height() / self.original_image_size.height()

            for ann in self.annotations:
                x1, y1, x2, y2, conf, cls = ann
                class_name = self.inference_module.model.names[int(cls)]  # Obtenir le nom de la classe
                # Appliquer l'échelle aux coordonnées
                scaled_x1 = int(x1 * scale_x)
                scaled_y1 = int(y1 * scale_y)
                scaled_width = int((x2 - x1) * scale_x)
                scaled_height = int((y2 - y1) * scale_y)
                painter.drawRect(scaled_x1, scaled_y1, scaled_width, scaled_height)
                
                # Ajouter le label de classe
                painter.drawText(scaled_x1, scaled_y1 - 5, f"{class_name} {conf:.2f}")

def main():
    app = QApplication(sys.argv)
    gui = InferenceGUI()
    gui.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
