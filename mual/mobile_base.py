#!/usr/bin/env python
# coding: utf-8

"""
Robot Mobile Control System

Desired features:
1. Control of a 4-wheel holonomic mobile robot.
2. Communication with an orchestration PC via ZeroMQ, mimicking ROS-style communication.
3. Speed and position control of motors via VESC controllers.
4. Possibility of control by joystick or network commands.
5. Inverse kinematics to convert desired movements into wheel speeds.
6. Publication of telemetry data (RPM, position, duty cycle, etc.).
7. Ability to switch between joystick and network command control via a network command.
8. Priority given to enable/disable commands.
9. Use of a Node system for communication between components.
10. Optional control of a robotic arm, if present and responsive.

Constraints:
1. Use of Python with specific libraries (PyVESC, ZeroMQ, PyGame).
2. Real-time communication with motor controllers.
3. Management of multiple threads for control and communication.
4. Speed and duty cycle limits for safety.
5. Use of joystick as default control source at startup.
6. Enable/disable commands are processed immediately, before any other action.
7. Use of the Node system for internal and external communication.
8. The system should start and operate even if the robotic arm is not present or not responding.
9. Strict speed limits must be enforced for all arm movements (move_speed parameter) for safety.

Potential issues and considerations:
1. Handling communication errors with VESCs.
2. Synchronization between different motor controllers.
3. Network communication latency for remote control.
4. Accuracy of position control, especially for long movements.
5. Management of drift and odometry errors.
6. Robustness to joystick disconnections or errors.
7. Performance optimization for smooth real-time control.
8. Graceful degradation when the robotic arm is not available.
9. Safety considerations for arm movements, including speed limits and workspace boundaries.

Code structure:
- Node class: Base for ZeroMQ communication.
- SpeedController class: Speed control of a VESC motor.
- PositionController class: Position control, inherits from SpeedController.
- Robot class: Manages the entire robot, including controllers and kinematics.

Important notes:
- The code uses an object-oriented approach with inheritance.
- Communication is based on a publish/subscribe model with ZeroMQ.
- Control can be switched between local joystick and network commands.
- Robot parameters (dimensions, wheel characteristics) are defined in an external configuration file.
- The robotic arm control is optional and the system should function without it.

TODO / Possible improvements:
- Implement full PID control for better precision.
- Add unit and integration tests.
- Improve error handling and robustness.
- Optimize performance, especially for control loops.
- Add more detailed documentation for each class and method.
- Implement more sophisticated error handling for the robotic arm.
"""

import logging
import math
from math import copysign
import traceback
from queue import Queue
import queue

import yaml
from mual import conf
from mual.control.holonomic import inverse_kinematics
from mual.control.node import Node
import zmq
import time
import threading
from pyvesc import VESC
import sys
import subprocess
from mual.control.joy import JoyNode
from xarm.wrapper import XArmAPI
import json

zmq_context = zmq.Context()

class SpeedController(Node):
    def __init__(self, name, vesc_path, parent=None):
        super().__init__(name, parent)
        self.rpm_goal = 0
        self.k = 3e-7
        self.dt = 0.001
        retries = 5
        self.vesc = None
        while retries > 0 and self.vesc is None:
            try:
                self.vesc = VESC(serial_port=vesc_path)
            except Exception:
                if retries > 1:
                    logging.warning(f"Could not connect to {vesc_path}, retrying")
                    time.sleep(0.5)
                else:
                    logging.warning(f"Could not connect to {vesc_path}, failed")
                retries -= 1
                self.vesc = None
        self.current_duty_cycle = 0
        self.running = True
        self.vesc_path = vesc_path
        self.last_measurement = None
        self.last_measurement_time = None
        self.previous_measurement_time = None

        logging.info(f"{self.z_path} : Create and start thread for %s.", self.vesc_path)
        self.thread = threading.Thread(target=self.thread_function)
        self.thread.start()

    def thread_function(self):
        logging.info(f"{self.z_path}: Thread %s: started", self.vesc_path)
        time.sleep(1)
        while self.running:
            self.cycle()
            time.sleep(self.dt)

    def set_duty_cycle(self, val):
        if val > 0.5:
            val = 0.5
        if val < -0.5:
            val = -0.5
        self.vesc.set_duty_cycle(val)

    def set_target_rpm(self, value):
        self.rpm_goal = value

    def compute(self, rpm):
        # Only the P of the PID really
        err = self.rpm_goal - rpm
        delta = self.k * err
        return delta

    def cycle(self):
        try:
            self.last_measurement = self.vesc.get_measurements()
            self.previous_measurement_time = self.last_measurement_time
            self.last_measurement_time = time.time()
        except AttributeError as e:
            print(self.z_path, e)
            return
        if self.last_measurement is None:
            print(self.z_path, "None returned from VESC")
            return
        self.current_duty_cycle = self.current_duty_cycle + self.compute(self.last_measurement.rpm)
        self.current_duty_cycle = min(max(self.current_duty_cycle, -0.5), 0.5)
        self.set_duty_cycle(self.current_duty_cycle)
        self.publish("rpm_goal", self.rpm_goal)
        self.publish("current_duty_cycle", self.current_duty_cycle)
        if self.last_measurement is not None:
            self.publish("current_rpm", self.last_measurement.rpm)
            self.publish("tachometer", self.last_measurement.tachometer)
            self.publish("tachometer_abs", self.last_measurement.tachometer_abs)
            if self.last_measurement_time is not None and \
                self.previous_measurement_time is not None:
                self.publish("cycle_time", self.last_measurement_time - self.previous_measurement_time)

class PositionController(SpeedController):
    def __init__(self, name, vesc_path, parent):
        super().__init__(name, vesc_path, parent)
        self.start_position = None
        self.target_delta = None
        self.max_speed = 1e4
        self.tac_diff = 0  # Ajout de cet attribut

    def update(self):
        if self.last_measurement is None:
            return
        t = self.last_measurement.tachometer
        if self.start_position is None:
            self.start_position = t

        if self.target_delta is None:
            return

        target = self.start_position+self.target_delta
        mult = 20
        self.tac_diff = target - t  # Stockage de tac_diff comme attribut
        self.publish("tac_diff", self.tac_diff)
        rpm = min(self.max_speed, max(-self.max_speed, mult * self.tac_diff))
        self.set_target_rpm(rpm)




class Robot(Node):
    def __init__(self, name, robot_conf):
        super().__init__(name, zmq_port=conf.ZMQ_PORT)
        self.speed_controllers = list()
        self.pos_controllers = list()
        self.robot_conf = robot_conf
        
        # Création des contrôleurs de position avec le préfixe "base"
        for name, path in self.robot_conf.esc_names.items():
            print(name, path)
            controller = PositionController(f"base/{name}", path, parent=self)
            self.pos_controllers.append(controller)

        # Création du nœud joystick comme enfant
        self.joy_node = JoyNode("joy", parent=self)
        self.joy_thread = threading.Thread(target=self.joy_node.run)
        self.joy_thread.start()

        self.control_recv_queue = Queue()
        self.is_enabled = True
        self.enable_recv_queue = Queue()

        # Initialisation du bras
        self.arm = None
        self.arm_available = False
        try:
            self.arm = XArmAPI(robot_conf.arm_ip)
            self.arm.motion_enable(enable=True)
            self.arm.set_mode(0)
            self.arm.set_state(state=0)
            self.arm_available = True
            logging.info("Robotic arm initialized successfully.")
        except Exception as e:
            logging.warning(f"Failed to initialize robotic arm: {e}")
            logging.info("The system will continue to operate without arm control.")

        self.control_mode = "base" 
        self.last_arm_command_time = 0
        self.arm_command_interval = 0.1
        self.arm_speeds = [0, 0, 0, 0, 0, 0]

        # Modification de la configuration ZMQ pour le joystick
        self.joy_socket = None
        self.setup_joy_socket()

        # Ajout des configurations pour le bras
        self.arm_target_position = None
        self.arm_moving = False
        self.arm_speed = 50  # mm/s

        self.total_ticks_to_destination = 0  # Ajout de l'attribut pour suivre la somme

        # Ajout des attributs pour le thread de lecture de position du bras
        self.arm_position_thread = None
        self.arm_position_running = False
        self.arm_lock = threading.Lock()
        
        # Démarrer le thread de lecture de position si le bras est disponible
        if self.arm_available:
            self.arm_position_running = True
            self.arm_position_thread = threading.Thread(target=self.arm_position_thread_func)
            self.arm_position_thread.start()

        # Ajouter cette ligne pour publier le mode initial
        self.publish("control_mode", self.control_mode)

        self.last_joy_command_time = 0
        self.joy_command_timeout = 0.2  # 200ms

        self.gripper_state = "open"  # Track gripper state
        self.last_gripper_button_state = False  # For button state tracking

    def setup_joy_socket(self):
        """Configure et reconnecte le socket joystick"""
        if self.joy_socket:
            self.joy_socket.close()
        
        self.joy_socket = zmq_context.socket(zmq.SUB)
        self.joy_socket.connect(f"tcp://localhost:{conf.ZMQ_PORT}")
        self.joy_socket.setsockopt_string(zmq.SUBSCRIBE, "/"+robot_name+"/joy/joy_data")
        # Définir un timeout court pour la réception
        self.joy_socket.setsockopt(zmq.RCVTIMEO, 100)

    def control_recv_func(self):
        control_socket = zmq_context.socket(zmq.SUB)
        control_socket.connect(f"tcp://{conf.Orchestrator.external_ip}:{conf.ZMQ_PORT}")
        control_socket.setsockopt(zmq.SUBSCRIBE, f'/control/{self.name}'.encode())
        
        # Modification de la souscription pour le bras
        control_socket.setsockopt(zmq.SUBSCRIBE, f'/control/{self.name}/arm_pose'.encode())
        
        while True:
            topic, msg = control_socket.recv_multipart()
            print("Received: ", topic, msg)
            msg = yaml.safe_load(msg.decode('utf-8'))
            if topic == f"/control/{self.name}/enable".encode():
                self.enable_recv_queue.put(("enable", msg))
            elif topic == f"/control/{self.name}/new_position".encode():
                self.control_recv_queue.put(("position", msg))
            elif topic == f"/control/{self.name}/control_source".encode():
                self.control_recv_queue.put(("source", msg))
            elif topic == f"/control/{self.name}/arm_pose".encode():
                self.control_recv_queue.put(("arm_pose", msg))

    def process_joy_data(self, joy_data):
        axes = joy_data["axes"]
        buttons = joy_data["buttons"]
        
        # Gripper control with button 2
        if buttons[2] == 1 and not self.last_gripper_button_state:
            self.gripper_state = "closed" if self.gripper_state == "open" else "open"
            command = "CLOSE" if self.gripper_state == "closed" else "OPEN"
            self.publish("gripper/command", command)
        self.last_gripper_button_state = buttons[2]
        
        # Clear arm errors when button 3 is pressed
        if buttons[3] == 1 and self.arm_available:
            self.clear_arm_errors()
            logging.info("Clearing arm errors")
        
        # Change control mode
        if buttons[0] == 1:  # Button 0 for base control
            if self.control_mode != "base":
                self.control_mode = "base"
                if self.arm_available:
                    self.arm.set_mode(0)
                    self.arm.set_state(state=0)
                logging.info("Control mode: Base")
        elif buttons[1] == 1:  # Button 1 for arm control
            if self.control_mode != "arm" and self.arm_available:
                self.control_mode = "arm"
                self.arm.set_mode(5)  # Speed mode
                self.arm.set_state(state=0)
                logging.info("Control mode: Arm")
            elif not self.arm_available:
                logging.warning("Arm control requested but arm is not available.")
        
        self.publish("control_mode", self.control_mode)
        
        if self.control_mode == "arm" and self.arm_available:
            # Arm control logic
            y_speed = -axes[0] * 30 if abs(axes[0]) > 0.1 else 0  # Axis 0 for Y
            x_speed = -axes[1] * 30 if abs(axes[1]) > 0.1 else 0  # Axis 1 for X
            z_speed = -axes[4] * 30 if abs(axes[4]) > 0.1 else 0  # Axis 4 for Z
            
            # Control the last joint (tool rotation) with axis 4
            # Convert to radians per second, max speed of 0.5 rad/s
            tool_rotation_speed = -axes[3] * 0.5 if abs(axes[3]) > 0.1 else 0  # Axis 3 for tool rotation
            
            # Add minimum speed threshold
            min_speed = 5  # Minimum speed in mm/s
            min_rotation_speed = 0.05  # Minimum speed in rad/s
            x_speed = 0 if abs(x_speed) < min_speed else x_speed
            y_speed = 0 if abs(y_speed) < min_speed else y_speed
            z_speed = 0 if abs(z_speed) < min_speed else z_speed
            tool_rotation_speed = 0 if abs(tool_rotation_speed) < min_rotation_speed else tool_rotation_speed
            
            self.arm_speeds = [x_speed, y_speed, z_speed, 0, 0, tool_rotation_speed]
            self.publish("arm_speeds", self.arm_speeds)
            return 0, 0, 0  # No movement for mobile base
        else:
            # Base mobile control logic
            vel_side = -axes[1] * 0.5 if abs(axes[1]) > 0.1 else 0
            vel_forward = -axes[0] * 0.5 if abs(axes[0]) > 0.1 else 0
            rot = -axes[3] * 0.5 if abs(axes[3]) > 0.1 else 0
            return vel_side, vel_forward, rot

    def update_arm_control(self):
        current_time = time.time()
        if current_time - self.last_arm_command_time >= self.arm_command_interval:
            self.arm.vc_set_cartesian_velocity(speeds=self.arm_speeds, is_radian=True, is_tool_coord=True, duration=0.5)
            self.last_arm_command_time = current_time

    def get_joy_data(self):
        try:
            latest_data = None
            while True:
                try:
                    topic, msg = self.joy_socket.recv_multipart(flags=zmq.NOBLOCK)
                    latest_data = yaml.safe_load(msg.decode())
                except zmq.Again:
                    break
                except zmq.ZMQError as e:
                    if e.errno == zmq.ETERM:
                        # Le contexte a été terminé
                        return None
                    elif e.errno == zmq.EAGAIN:
                        # Pas de données disponibles
                        break
                    else:
                        # Autre erreur ZMQ, on essaie de reconnecter
                        logging.warning(f"ZMQ error: {e}, attempting to reconnect joy socket")
                        self.setup_joy_socket()
                        break
            return latest_data
        except Exception as e:
            logging.error(f"Error in get_joy_data: {e}")
            return None

    def move_arm_to_position(self, target_pose):
        """
        Move the arm to a target pose based on specified reference frame
        target_pose format: [x, y, z, rotation, frame_type]
        frame_type can be: 'absolute', 'base_relative', 'tool_relative'
        Returns True if movement was successful
        """
        if not self.arm_available:
            logging.warning("Arm movement requested but arm is not available")
            return False

        try:
            frame_type = target_pose[4] if len(target_pose) > 4 else 'absolute'
            position = target_pose[:4]  # x, y, z, rotation
            print(f"Arm moving to pose {position} ({frame_type} coordinates)")
            
            with self.arm_lock:
                # Obtenir la position actuelle pour les mouvements relatifs
                if frame_type != 'absolute':
                    code, current_pos = self.arm.get_position()
                    if code != 0:
                        logging.error("Failed to get current position")
                        return False
                    current_yaw = current_pos[5]  # yaw est à l'index 5
            
                if frame_type == 'absolute':
                    code = self.arm.set_position(x=position[0], 
                                              y=position[1], 
                                              z=position[2],
                                              yaw=position[3],  # rotation absolue
                                              is_radian=False,
                                              relative=False,
                                              move_speed=(10,0.1),
                                              wait=True)
                elif frame_type == 'base_relative':
                    target_yaw = position[3]
                    code = self.arm.set_position(x=position[0], 
                                              y=position[1], 
                                              z=position[2],
                                              yaw=target_yaw,  # rotation absolue calculée
                                              is_radian=False,
                                              relative=True,
                                              move_speed=(10,0.1),
                                              wait=True)
                elif frame_type == 'tool_relative':
                    code = self.arm.set_tool_position(x=position[0], 
                                              y=position[1], 
                                              z=position[2],
                                              yaw=position[3],  # rotation absolue calculée
                                              is_radian=False,
                                              move_speed=(10,0.1),
                                              wait=True)
                else:
                    logging.error(f"Unknown frame type: {frame_type}")
                    return False
                
                if code != 0:
                    logging.error(f"Failed to move arm: {code}")
                    return False
                
                return True

        except Exception as e:
            logging.error(f"Error moving arm: {e}")
            traceback.print_exc()
            return False

    def arm_position_thread_func(self):
        """Thread function to read and publish arm position"""
        logging.info("Starting arm position monitoring thread")
        while self.arm_position_running:
            try:
                with self.arm_lock:
                    code, position = self.arm.get_position()
                    if code == 0:  # Success
                        self.publish("arm_position", position)
                    else:
                        logging.warning(f"Failed to get arm position, error code: {code}")
            except Exception as e:
                logging.error(f"Error reading arm position: {e}")
                # Ajouter un petit délai en cas d'erreur pour éviter une boucle d'erreurs trop rapide
                time.sleep(0.1)
            
            # Sleep to maintain ~50Hz reading rate
            time.sleep(0.02)  # 1/50 second
        
        logging.info("Arm position monitoring thread stopped")

    def main_control_loop(self):
        recv_thread = threading.Thread(target=self.control_recv_func)
        recv_thread.start()
        target_pos = [0, 0, 0]
        wheels_delta = [0, 0, 0, 0]

        try:
            while True:
                # Traiter d'abord les commandes enable
                try:
                    while not self.enable_recv_queue.empty():
                        cmd_type, cmd_data = self.enable_recv_queue.get_nowait()
                        if cmd_type == "enable":
                            self.is_enabled = bool(cmd_data)
                            if not self.is_enabled:
                                # Arrêter immédiatement tous les moteurs
                                for sc in self.pos_controllers:
                                    sc.set_target_rpm(0)
                except queue.Empty:
                    pass

                # Ajouter une vérification périodique de la connexion
                if not self.joy_socket:
                    self.setup_joy_socket()
                
                joy_data = self.get_joy_data()
                current_time = time.time()
                if joy_data and (abs(joy_data["axes"][0]) > 0.1 or 
                                 abs(joy_data["axes"][1]) > 0.1 or 
                                 abs(joy_data["axes"][3]) > 0.1):
                    self.last_joy_command_time = current_time
                joy_active = (current_time - self.last_joy_command_time) < self.joy_command_timeout
                self.publish("joy_active", joy_active)

                if self.is_enabled:
                    # Traiter les commandes réseau uniquement si le joystick n'est pas actif
                    if not joy_active:
                        try:
                            while not self.control_recv_queue.empty():
                                cmd_type, cmd_data = self.control_recv_queue.get_nowait()
                                if cmd_type == "position":
                                    target_pos = cmd_data
                                    d = [target_pos[i] for i in range(3)]
                                    wheels_delta = inverse_kinematics(d[0], d[1], d[2],
                                                                  self.robot_conf.width,
                                                                  self.robot_conf.length,
                                                                  self.robot_conf.wheel_radius)
                                    for wheel, sc in zip(wheels_delta, self.pos_controllers):
                                        tick_per_rad = self.robot_conf.Poupette.encoder_tick_per_turn / (math.pi * 2)
                                        sc.target_delta = wheel * tick_per_rad
                                        sc.start_position = None
                                elif cmd_type == "arm_pose" and self.arm_available:
                                    # Toujours traiter les commandes du bras
                                    self.arm.set_mode(0)
                                    self.arm.set_state(state=0)
                                    self.publish("arm_position_reached", False)
                                    success = self.move_arm_to_position(cmd_data)
                                    if success:
                                        self.arm.set_mode(5)
                                        self.arm.set_state(state=0)
                                    self.publish("arm_position_reached", success)
                                elif cmd_type == "gripper":
                                    # Handle gripper commands from sequences
                                    self.publish("gripper/command", cmd_data)
                                    self.gripper_state = "closed" if cmd_data == "CLOSE" else "open"
                        except queue.Empty:
                            pass

                    # Traiter les commandes du joystick
                    if joy_data:
                        vel_side, vel_forward, rot = self.process_joy_data(joy_data)
                        if self.control_mode == "base" and joy_active:
                            cmd_joy = [vel_side * 30, vel_forward * 30, rot * 40]
                            cmd_joy.extend([self.robot_conf.width, self.robot_conf.length, self.robot_conf.wheel_radius])
                            wheels_speed = inverse_kinematics(*cmd_joy)
                            
                            for wheel, sc in zip(wheels_speed, self.pos_controllers):
                                target_rpm = wheel * 60
                                sc.set_target_rpm(target_rpm)
                                sc.target_delta = None
                        elif self.control_mode == "arm" and joy_active:
                            self.update_arm_control()
                    elif not joy_active:
                        # Utiliser le contrôle en position seulement si le joystick n'est pas actif
                        for sc in self.pos_controllers:
                            sc.update()
                        
                else:
                    for sc in self.pos_controllers:
                        sc.set_target_rpm(0)
                    if self.control_mode == "arm":
                        self.arm.vc_set_cartesian_velocity(speeds=[0, 0, 0, 0, 0, 0], duration=0.1)
                        self.arm_speeds = [0, 0, 0, 0, 0, 0]
                        self.publish("arm_speeds", self.arm_speeds)

                # Calculer la somme des tac_diffs
                self.total_ticks_to_destination = sum(abs(controller.tac_diff) 
                                                    for controller in self.pos_controllers)
                # Publier la somme
                self.publish("base/ticks_to_destination", self.total_ticks_to_destination)

                time.sleep(0.01)  # Réduire à 10ms pour une mise à jour plus fréquente

        except Exception as e:
            print("Finished", e)
            print(traceback.format_exc())
            for sc in self.pos_controllers:
                sc.set_target_rpm(0)
            if self.control_mode == "arm":
                self.arm.vc_set_cartesian_velocity(speeds=[0, 0, 0, 0, 0, 0], duration=0.1)
                self.arm.set_mode(0)
                self.arm.set_state(state=0)
        finally:
            self.joy_node.stop()
            self.joy_thread.join()
            if self.arm_available:
                self.arm.disconnect()

    def __del__(self):
        # Stop the arm position thread before cleanup
        if hasattr(self, 'arm_position_running'):
            logging.info("Stopping arm position monitoring")
            self.arm_position_running = False
            if hasattr(self, 'arm_position_thread') and self.arm_position_thread:
                self.arm_position_thread.join(timeout=2.0)
                if self.arm_position_thread.is_alive():
                    logging.warning("Arm position thread did not stop cleanly")
        
        # Existing cleanup code
        if hasattr(self, 'arm'):
            self.arm.disconnect()

    def clear_arm_errors(self):
        if self.arm_available:
            with self.arm_lock:
                self.arm.clean_error()
                self.arm.motion_enable(enable=True)
                self.arm.set_mode(0)
                self.arm.set_state(state=0)
                time.sleep(0.1)
                self.arm.set_mode(5)
                self.arm.set_state(state=0)

            logging.info("Arm errors cleared and state reset to ready")
        else:
            logging.warning("Attempted to clear arm errors, but arm is not available")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python mobile_base.py <robot_name>")
        sys.exit(1)
    robot_name = sys.argv[1]
    robot_conf = getattr(conf, robot_name.capitalize())
    robot = Robot(robot_name, robot_conf)
    robot.main_control_loop()
