# MUAL

This is a home for the source used in the [MUAL](https://mual.fr) project as I am moving away from ROS the [ArmStone](https://github.com/JuliusSustarevas/ArmStone) code base that inspired the initial effort.

## Project Structure

- `mobile_base.py` is the script to start on the robot.
- `orchestrator_gui.py` is the script to start on the orchestrator PC.
- `vision_training_gui.py` starts a software to train the vision model.

- `vision/` contains the code for the vision modules for inference and training.
- `control/` contains the code for the holonomic base controls.
- `tools/` contains various notebooks and test scripts.
- `website/` is the source for mual.fr

## Installation

To install MUAL, you can use pip with various options depending on your needs:

1. Basic installation (for robot control only):
   `pip install .`

2. Installation with vision capabilities:
   `pip install .[vision]`

3. Installation with UI capabilities:
   `pip install .[ui]`

4. Full installation (all features):
   `pip install .[full]`

5. Installation for development:
   `pip install .[dev]`

6. Editable installation (for development):
   `pip install -e .[full,dev]`

## Usage

- To start the robot control:
  `python mobile_base.py`

- To start the orchestrator GUI:
  `python orchestrator_gui.py`

- To start the vision training GUI:
  `python vision_training_gui.py`

